-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2021 at 01:22 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '$2y$10$seIxBLKB633S2nK2dtWhBe1iIVlQNAf/tnVi6ksA9wWyukLfHdnhC', '2021-06-05 00:23:46', '2021-06-05 00:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `airport_taxi`
--

CREATE TABLE `airport_taxi` (
  `id` int(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `capacity` varchar(255) NOT NULL,
  `price_upto_300` int(255) NOT NULL,
  `price_after_300` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `airport_taxi`
--

INSERT INTO `airport_taxi` (`id`, `img`, `name`, `capacity`, `price_upto_300`, `price_after_300`) VALUES
(5, 'gallery/Etios.jpg', 'SEDAN (ETIOS)', '4+1', 899, '100'),
(7, 'gallery/innova.jpg', 'SUV (INNOVA)', '7+1', 1599, '100'),
(8, 'gallery/crysta.jpg', 'SUV (INNOVA CRYSTA)', '7+1', 1799, '100'),
(9, 'gallery/Maruti-Suzuki-Swift-Dzire-White-639x397.png', 'SEDAN (DZIRE)', '4+1', 899, '100');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(255) NOT NULL,
  `bid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `anumber` varchar(255) DEFAULT NULL,
  `pickup` varchar(255) NOT NULL,
  `drop` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `package` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `tc` varchar(255) NOT NULL,
  `bdt` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `edate` varchar(255) NOT NULL,
  `btype` varchar(255) NOT NULL,
  `driver` varchar(255) DEFAULT NULL,
  `feedback` varchar(255) DEFAULT NULL,
  `instruction` text DEFAULT NULL,
  `book_by` varchar(255) NOT NULL,
  `ptype` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `bid`, `name`, `email`, `number`, `anumber`, `pickup`, `drop`, `date`, `time`, `package`, `price`, `tc`, `bdt`, `status`, `edate`, `btype`, `driver`, `feedback`, `instruction`, `book_by`, `ptype`) VALUES
(1, 'GMT-1', 'Ganesh', 'ananth.anu35@gmail.com', '7892875546', '', 'BTM 2nd Stage, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-14', '10:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-13 06:46:43', 'Done', '', 'Offline', 'Vikash Yadav', '					', '  ', 'Ananth', 'Cash'),
(2, 'GMT-2', 'Anil Kumar ', 'anil.saiagencies@gmail.com', '8005841165', '9742801707', 'Magadi Road, Lakshminarayanapuram, Binnipete, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-15', '03:30', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-14 08:27:50', 'Done', '', 'Online', 'SRIDHAR', 'Everything went good', '', '', ''),
(3, 'GMT-3', 'P Satheesh babu', 'satheesh.pulluru385@gmail.com', '9742667638', '9060645730', 'P.Kothakota, Kothakota, Andhra Pradesh, India', 'Electronic City Phase II, Electronic City, Bengaluru, Karnataka, India', '2020-11-16', '13:00', 'Outstation', '4242', 'HATCHBACK-4+1', '2020-11-14 10:16:27', 'Customer Cancelled', '2020-11-16', 'Online', '', '															', '    ', '', ''),
(4, 'GMT-4', 'Khevender', 'ananth.anu35@gmail.com', '8800319898', '', 'Carmelaram, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-15', '13:00', 'Airport', '899', 'HATCHBACK-4+1', '2020-11-14 20:00:32', 'Done', '', 'Offline', 'SRIDHAR', '					', '  collect 800/- only and take non toll route', 'Ananth', 'Cash'),
(5, 'GMT-5', 'Ms Rashi', 'rashi.chandram04@gmail.com', '8553402427', '9739979428', 'GR Queens Pride, Koppa Rd, Suraksha nagar, Yelenahalli, Begur, Bengaluru, Karnataka 560068', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-20', '10:30', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-16 06:08:27', 'Done', '', 'Offline', 'Anil', '					', 'Send Good Car', 'Aishwwarya M', 'Cash'),
(6, 'GMT-6', 'Jalandhar LG', 'jaalandharalg@gmail.com', '9880410052', '', 'Kudlu Gate, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-17', '06:30', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-16 09:22:27', 'Done', '', 'Online', 'Chandra', 'Everything Is Fine.				', '', '', ''),
(7, 'GMT-7', 'Deepak Kumar mohapatra', 'depak.mohapatra13@gmail.com', '09701813556', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Chandapura, Bommasandra, Karnataka, India', '2020-11-16', '22:33', 'Airport', '1099', 'HATCHBACK-4+1', '2020-11-16 10:06:23', 'Done', '', 'Online', '', 'Everything Is Fine.		', '  OTHER CABS', '', ''),
(8, 'GMT-8', 'Guest', 'mm@gmail.com', '8130457750', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Kormangala, Bengaluru, Karnataka, India', '2020-11-16', '22:47', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-16 10:18:46', 'Done', '', 'Offline', '', 'Customer Not Responding For The Call.	', '    OTHER CABS', 'Manjunath B S', 'Cash'),
(9, 'GMT-9', 'Mr. Rahual', 'rmahanta989@gmail.com', '9148069364', '', '6th main 18th arch cross BTM 2nd Stage, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India and Back', '2020-11-17', '03:30', 'Airport', '1399', 'SUV MINI (ERITIGA)-6+1', '2020-11-16 10:24:09', 'Done', '', 'Offline', 'SRIDHAR', 'Call Dint Connected.', '', 'Manjunath B S', 'Cash'),
(10, 'GMT-10', 'Ms. Sangeetha', 'sushree1986@gmail.com', '9620370660', '9886104517', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Begur - Koppa Road, Akshayanagara East, Akshayanagar, Bengaluru, Karnataka, India', '2020-11-17', '12:30', 'Airport', '1499', 'SUV MINI (ERITIGA)-6+1', '2020-11-16 10:56:03', 'Done', '', 'Offline', '', 'Everything Is Fine.', '   Charge 1400 only take non toll route.   Name : syed Sadiq\r\nPh  N  : 9663819822\r\nCAR N  :KA50 9423\r\ná´„á´€Ê™ á´›.    ÉªÉ´É´á´á´ á´€ ', 'Manjunath B S', 'Cash'),
(11, 'GMT-11', 'JJ Sonawala', 'bit2304sun@hotmail.com', '9870825000', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Whitefield, Bengaluru, Karnataka, India', '2020-11-22', '10:15', 'Airport', '1499', 'SUV MINI (ERITIGA)-6+1', '2020-11-16 12:38:51', 'Customer Cancelled', '', 'Online', '', '					', '', '', ''),
(13, 'GMT-12', 'ganesh', 'manjunath.23.6@gmail.com', '7406182704', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Whitefield, Bengaluru, Karnataka, India', '2020-11-22', '09:31', 'Airport', '1499', 'SUV MINI (ERITIGA)-6+1', '2020-11-17 02:14:01', 'Customer Cancelled', '', 'Online', '', '					test booking', '', '', ''),
(14, 'GMT-13', 'Mr. JJ Sonawala', 'bit2304sun@hotmail.com', '9870825000', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Sheraton Grand Prestige Shantiniketan Hoodi, Whitefield, Bengaluru, Karnataka 560048', '2020-11-22', '10:15', 'Airport', '1300', 'SUV MINI (ERITIGA)-6+1', '2020-11-17 04:23:34', 'Done', '', 'Offline', 'BHIMURAYA', '					', '  ', 'Manjunath B S', 'Cash'),
(15, 'GMT-14', 'Mr. JJ Sonawala', 'bit2304sun@hotmail.com', '9870825000', '', 'Orion Mall Dr Rajkumar Rd, Rajajinagar, Bengaluru, Karnataka 560055', 'Kempegowda International Airport Bengaluru, KIAL Rd, Devanahalli, Bengaluru, Karnataka 560300', '2020-11-25', '08:45', 'Airport', '1300', 'SUV MINI (ERITIGA)-6+1', '2020-11-17 04:27:37', 'Customer Cancelled', '', 'Offline', '', 'Plan Cancelled ', '      ', 'Manjunath B S', 'Cash'),
(16, 'GMT-15', 'Mr. Vishnu Dixit', 'ananth.anu35@gmail.com', '7355321474', '9880695471', 'om shakthi layout gb palya', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-18', '08:30', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-17 18:26:50', 'Done', '', 'Offline', 'SRIDHAR', 'Call Dint Connected.', '', 'Manjunath B S', 'Cash'),
(17, 'GMT-16', 'Mr. Manjunath', 'manjunath.23.6@gmail.com', '7406001992', '', 'BTM 2nd Stage, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-18', '07:14', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-17 18:44:22', 'Customer Cancelled', '', 'Offline', '', '					', '', 'Manjunath B S', 'Cash'),
(18, 'GMT-17', 'Vikash Yadav', 'vy8412@gmail.com', '8319278218', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Hebbal, Bengaluru, Karnataka, India', '2020-11-18', '10:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-17 20:02:38', 'Customer Cancelled', '', 'Offline', 'Vikash Yadav', '										', '', 'Vikash Yadav', 'Cash'),
(19, 'GMT-18', 'Mr. Vikash sachdev', 'vikashsachdev@gmail.com', '9880463833', '9008868855', 'SSVR tridax balgere road, Varthur, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-30', '01:45', 'Airport', '899', 'HATCHBACK-4+1', '2020-11-17 23:41:12', 'Done', '', 'Online', 'Ganesh', '					', '    ', '', ''),
(20, 'GMT-19', 'Rhea Gurung', 'rheabts23@gmail.com', '7899740047', '9606965872', 'Marathahalli, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-22', '08:30', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-18 01:22:43', 'Customer Cancelled', '', 'Online', '', 'Plan changed ', '', '', ''),
(21, 'GMT-20', 'Mr Suman', 'lekh.prasad@gmail.com', '7676889304', '', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli,  and Back', '2020-11-19', '14:30', 'Airport', '1099', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-18 07:00:47', 'Taxi Not Available', '', 'Offline', '', 'Wrong booking', '  Collect 1350 plus Toll 95+95', 'Ananth', 'Cash'),
(22, 'GMT-21', 'Ms. Ayushi Ghosal', 'ananth.anu35@gmail.com', '9108344372', '', 'Little Berries playschool 342, 1st A Main Road, Koramangala 8th Block, Koramangala', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-19', '05:00', 'Airport', '700', 'HATCHBACK-4+1', '2020-11-18 07:18:28', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Ananth', 'Cash'),
(23, 'GMT-22', 'Mr Suman', 'lekh.prasad@gmail.com', '7676889304', '', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli,  and Back', '2020-11-19', '14:30', 'Airport', '999', 'HATCHBACK-4+1', '2020-11-18 07:20:16', 'Taxi Not Available', '', 'Offline', '', 'wrong Booking', '    Collect 1350 plus Toll 95+95  ', 'Ananth', 'Cash'),
(24, 'GMT-23', 'Mr Suman', 'lekh.prasad@gmail.com', '7676889304', '', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli,  and Back', '2020-11-19', '14:30', 'Airport', '1350', 'HATCHBACK-4+1', '2020-11-18 07:29:42', 'Done', '', 'Offline', 'SRIDHAR', '					', '     Collect 1350 plus Toll 95+95   ', 'Ananth', 'Cash'),
(25, 'GMT-24', 'Sudeep ps', 'sudeep.ps317@gmail.com', '8095219022', '8618714915', 'Nagarbhavi, Bengaluru, Karnataka, India', 'New Airport Road, HBR Layout 4th Block, Kalyan Nagar, Bangalore, Karnataka, India', '2020-11-19', '06:44', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-18 08:15:07', 'Customer Cancelled', '', 'Online', '', '					', '', '', ''),
(27, 'GMT-25', 'Mr. Rajdeep', 'rajmiraskar@gmail.com', '8073263338', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Basveswar nagar, kurabarahalli ', '2020-11-19', '11:00', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-18 18:51:58', 'Done', '', 'Offline', 'SRIDHAR', '					', '      ', 'Manjunath B S', 'Cash'),
(28, 'GMT-26', 'Mr Madhavrao', 'madhvarao.unnamca111@gmail.com', '8143171431', '', 'Sai thanmai pg for gents, #12 vivekananda layout opp home town marathalli', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-20', '04:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-18 20:24:05', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(29, 'GMT-27', 'Sanjeev Yadav', 'shubhamdayal.jind@gmail.com', '9812065150', '8295611070', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Byagadadhenahalli, Karnataka, India', '2020-11-19', '23:00', 'Airport', '1299', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-19 03:32:00', 'Done', '', 'Online', 'Anil', '					', '', 'Online', ''),
(30, 'GMT-28', 'Sanjeev Yadav', 'shubhamdayal.jind@gmail.com', '9812065150', '8295611070', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Byagadadhenahalli, Karnataka, India', '2020-11-19', '23:00', 'Airport', '1299', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-19 03:54:00', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(31, 'GMT-29', 'Mr Vinodh Kumar', 'ananth.anu35@gmail.com', '9742976404', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Kormangala Passport Office', '2020-11-20', '09:30', 'Airport', '750', 'HATCHBACK-4+1', '2020-11-19 06:51:20', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Ananth', 'Cash'),
(32, 'GMT-30', 'Vivek Yadav', 'vivekchandyadav323@gmail.com', '9621145255', '9476802240', 'Vns airport ', 'Azamgarh, Uttar Pradesh, India', '2020-11-27', '19:54', 'Airport', '2199', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-19 07:25:13', 'Customer Cancelled', '', 'Online', '', 'wrong booking', '', 'Online', ''),
(33, 'GMT-31', 'Ms Shusmitha', 'ritu1ganguly@gmail.com', '9916506358', '', 'Munnekollal Marathahalli  ', 'Kempegowda International Airport Bengaluru, KIAL Rd, Devanahalli, Bengaluru, Karnataka 560300', '2020-11-25', '04:30', 'Airport', '750', 'HATCHBACK-4+1', '2020-11-20 02:47:41', 'Customer Cancelled', '', 'Offline', '', '					', '  ', 'Aishwwarya M', 'Cash'),
(34, 'GMT-32', 'Mr Vinod', 'vinodtm89@yahoo.com', '9901952757', '', '#205, Sripaadam Gardens, TK Deepak Layout Jp nagar 8th phase Bangalore 560083', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-21', '03:00', 'Airport', '1250', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-20 04:09:12', 'Done', '', 'Offline', 'SRIDHAR', '					', '  2nd Pick Up Opp East West Academy 5th Block,Rajajinagar, ', 'Aishwwarya M', 'Cash'),
(35, 'GMT-33', 'Ravi', 'Ravikumar3103@gmail.com', '8884340900', '8884340900', 'Gunjur, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-21', '15:00', 'Airport', '899', 'HATCHBACK-4+1', '2020-11-21 02:24:22', 'Done', '', 'Online', '', '					', '  Shiva KA 03 AD 8939 9632117043', 'Online', ''),
(36, 'GMT-34', 'Ms. Ayushi Ghosal', 'ananth.anu35@gmail.com', '9108344372', '', '  Little Berries playschool 342, 1st A Main Road, 8th Block, Koramangala', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-22', '11:55', 'Airport', '700', 'HATCHBACK-4+1', '2020-11-21 07:20:38', 'Done', '', 'Offline', 'SRIDHAR', '					', '  ', 'Manjunath B S', 'Cash'),
(37, 'GMT-35', 'Shakeel Mir', 'mirshakeel25@gmail.com', '0523614070', '00971523614070', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Kumaraswamy Layout, Bengaluru, Karnataka, India', '2020-11-22', '15:45', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-21 10:43:01', 'Done', '', 'Online', 'SRIDHAR', '					', '    ', 'Online', ''),
(38, 'GMT-36', 'vinamra singhai', 'vinsin328@gmail.com', '9986226532', '9986226532', 'Koramangala 1st Block, HSR Layout 5th Sector, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-22', '12:15', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-21 10:56:13', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(39, 'GMT-37', 'Chinmay Shastri', 'Chinmay.shastri@gmail.com', '00447830010030', '9902405258', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Banavasi, Karnataka, India', '2020-11-23', '05:30', 'Airport', '7999', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-21 11:50:33', 'Done', '', 'Online', 'Ganesh', '					', '', 'Online', ''),
(40, 'GMT-38', 'Mr. Sonawala', 'bit2304sun@hotmail.com', '8169017798', '', 'Sheraton Grand Bengaluru Whitefield Hotel & Convention Center, Prestige Shantiniketan Hoodi, Whitefield, Bengaluru, Karnataka 560048', 'Orion Mall, Dr Rajkumar Rd, Rajajinagar, Bengaluru, Karnataka 560055', '2020-11-23', '15:45', 'Local', '1000', 'SUV (INNOVA)-7+1', '2020-11-21 22:44:32', 'Done', '', 'Offline', 'Chandu', '					', ' Point to point ', 'Manjunath B S', 'Cash'),
(41, 'GMT-39', 'Ms. Shubhra', 'shubhrasurolia@gmail.com', '9739001027', '', 'Villa# 275, Daddy garden Phase 2 Kammasandra Road Electronic city', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-23', '07:45', 'Airport', '950', 'HATCHBACK-4+1', '2020-11-22 00:12:01', 'Done', '', 'Offline', 'BHIMURAYA', '					', '', 'Manjunath B S', 'Cash'),
(42, 'GMT-40', 'Mr. Abhishek', 'abhishek.kalra1992@gmail.com', '9900378650', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Eijipura Bus Stop', '2020-11-22', '23:15', 'Airport', '750', 'HATCHBACK-4+1', '2020-11-22 00:30:29', 'Done', '', 'Offline', '', '					', '  praveen 7981425705 KA 51 D 0330', 'Manjunath B S', 'Cash'),
(43, 'GMT-41', 'Mr. Kirtan Sahu', 'kirtansahu.87@gmail.com', '9738055876', '', '#2, Block# 142, UNR Street, Munekolla Marathalli', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-28', '13:00', 'Airport', '850', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-22 00:55:33', 'Done', '', 'Offline', 'SRIDHAR', '					', ' Near UNR Ration Shop ', 'Manjunath B S', 'Cash'),
(44, 'GMT-42', 'Ananth Nayak', 'ananth.anu35@gmail.com', '9611673611', '9886131171', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Mantri Paradise Bannerghatta Main Rd, Omkar Nagar,', '2020-11-23', '14:00', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-22 04:13:55', 'Done', '', 'Offline', 'Anil', '					', '', 'Ananth', 'Cash'),
(45, 'GMT-43', 'Mr. Chandan', 'cgupta101@gmail.com', '9779961139', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Snn Raj Seranity yelanahalli, Akshynagar', '2020-11-22', '19:30', 'Airport', '750', 'HATCHBACK-4+1', '2020-11-22 06:52:57', 'Done', '2020-11-22', 'Offline', '', '					', '     KA 05 A F8595\r\n9916690752 Yuvraj ', 'Manjunath B S', 'Cash'),
(46, 'GMT-44', 'Mr. Shyamal', 'mm@gmail.com', '9110897178', '', 'S.P Road Cross, Kumbarpet, Dodpete, Nagarathpete, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-23', '02:00', 'Airport', '750', 'HATCHBACK-4+1', '2020-11-22 07:18:44', 'Done', '', 'Offline', 'SRIDHAR', '					', '  ', 'Manjunath B S', 'Cash'),
(47, 'GMT-45', 'Venkatesh', 'a.venkatg@gmail.com', '9590254466', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Arekere, Bengaluru, Karnataka, India', '2020-11-22', '20:09', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-22 07:40:42', 'Done', '', 'Online', '', '					', '  Other cabs done', 'Online', ''),
(48, 'GMT-46', 'Nitish ', 'nitishb175@gmail.com', '9379188543', '9008167569', 'Rajarajeshwari Nagar, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-26', '02:10', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-22 07:51:39', 'Done', '', 'Online', 'SRIDHAR', '					', '', 'Online', ''),
(49, 'GMT-47', 'Ms. Ayushi Ghosal', 'ananth.anu35@gmail.com', '9108344372', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '  Little Berries playschool 342, 1st A Main Road, 8th Block, Koramangala', '2020-11-24', '00:45', 'Airport', '700', 'HATCHBACK-4+1', '2020-11-23 05:43:51', 'Done', '', 'Offline', '', '					', '        Booking Done Other cabs', 'Manjunath B S', 'Cash'),
(50, 'GMT-48', 'Sharath', 'sharusharathnaidu4284@gmail.com', '9353676403', '7090746747', 'Goshala Doddanekundi ', 'Majestic railway station ', '2020-11-23', '20:13', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-23 07:44:44', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(51, 'GMT-49', 'Mohamed Ismail', 'tryinkalwas@gmail.com', '09047942452', '', 'Kempegowda International Airport Road, Yerthiganahalli, à®•à®°à¯à®¨à®¾à®Ÿà®•, à®‡à®¨à¯à®¤à®¿à®¯à®¾', 'Yeshvanthapura, à®•à®°à¯à®¨à®¾à®Ÿà®•, à®‡à®¨à¯à®¤à®¿à®¯à®¾', '2020-11-23', '21:20', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-23 08:44:40', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(52, 'GMT-50', 'Mr. Sonawala', 'bit2304sun@hotmail.com', '8169017798', '', 'Sheraton Grand Bangalore Hotel at Brigade Gateway', 'Hebbal, Bengaluru, Karnataka', '2020-11-24', '15:00', 'Local', '500', 'SEDAN(DZIRE ETIOS)-4+1', '2020-11-23 11:25:03', 'Customer Cancelled', '', 'Offline', 'SRIDHAR', 'Plan Changed ', '  Point to point  ', 'Ananth', 'Cash'),
(53, 'GMT-51', 'Mr. Sonawala', 'bit2304sun@hotmail.com', '8169017798', '', 'Hebbal, Bengaluru, Karnataka', 'Sheraton Grand Bangalore Hotel at Brigade Gateway', '2020-11-24', '18:00', 'Local', '500', 'SEDAN(DZIRE ETIOS)-4+1', '2020-11-23 11:26:20', 'Customer Cancelled', '', 'Offline', 'SRIDHAR', 'Plan Changed ', '   Point to point   ', 'Ananth', 'Cash'),
(54, 'GMT-52', 'Mr. Sonawala', 'bit2304sun@hotmail.com', '8169017798', '', 'Sheraton Grand Bangalore Hotel at Brigade Gateway Rajkumar Road Malleswaram, Rajajinagar, Bengaluru, Karnataka 560055', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-25', '08:45', 'Airport', '1299', 'SUV MINI (ERITIGA)-6+1', '2020-11-23 11:28:24', 'Done', '', 'Offline', 'Sunil kumar', '					', '  ', 'Ananth', 'Cash'),
(55, 'GMT-53', 'Mr Ayush', 'ayush.niks@gmail.com', '8050115941', '8789748262', 'Raycon Lotus Apartment, AECS Layout - A Block, AECS Layout, Brookefield, Bengaluru, Karnataka 560037', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-26', '05:30', 'Airport', '850', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-23 22:04:15', 'Done', '', 'Offline', 'Ganesh', '					', '  ', 'Ananth', 'Cash'),
(56, 'GMT-54', 'Mr. T R Sangar', 'trsangar@gmail.com', '8360011460', '', 'Prestige Lakeside Habitat, 28/2, SH 35, Devasthanagalu, Gunjur Village, Bengaluru, Karnataka 560087', 'Kempegowda International Airport Bengaluru, KIAL Rd, Devanahalli, Bengaluru, Karnataka 560300', '2020-11-26', '07:30', 'Airport', '750', 'HATCHBACK-4+1', '2020-11-25 03:31:55', 'Taxi Not Available', '', 'Offline', 'SRIDHAR', '					', ' Take non toll route', 'Manjunath B S', 'Cash'),
(57, 'GMT-55', 'Mr. Vivek', 'vivekanand2892@gmail.com', '8797576739', '', 'Nagappareddy Layout, Vignan Nagar, Doddanekundi, Doddanekkundi, Bengaluru, Karnataka 560093', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-26', '13:30', 'Airport', '850', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-26 00:27:24', 'Done', '', 'Offline', '', '					', '  Done by  w07', 'Manjunath B S', 'Cash'),
(58, 'GMT-56', 'Nagaraj G', 'nagarajg63@gmail.com', '9740194154', '9035014154', 'Subbannaiah Palya, Banaswadi, Bengaluru, Karnataka, India', 'Bangalore international airport', '2020-11-30', '08:15', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-27 02:33:33', 'Done', '', 'Online', 'Ganesh', '					', '  ', 'Online', ''),
(59, 'GMT-57', 'Laxmi Arya', 'differentstyles@gmail.com', '8892780164', '9738862212', 'Satpur Colony, Nashik, Maharashtra, India', 'Nasik Airport Building, Viman Nagar, Ojhar, Maharashtra, India', '2020-12-02', '08:15', 'Airport', '799', 'HATCHBACK-4+1', '2020-11-27 02:56:05', 'Customer Cancelled', '', 'Online', '', '										', '', 'Online', ''),
(60, 'GMT-58', 'Ms Bipasha Agarwal', 'agarwalbipasha@gmail.com', '9883097200', '', 'Premkamal Residency 154, Adugodi Main Rd, Koramangala 8th Block, Koramangala, Bengaluru, Karnataka 560095', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-28', '18:00', 'Airport', '749', 'HATCHBACK-4+1', '2020-11-27 05:04:24', 'Done', '', 'Offline', 'SRIDHAR', '					', '  ', 'Aishwwarya M', 'Cash'),
(61, 'GMT-59', 'Mr  Varun', 'varun.sood@yahoo.com', '7022277279', '9779664322', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-09', '06:00', 'Airport', '949', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-27 06:06:32', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Aishwwarya M', 'Cash'),
(62, 'GMT-60', 'Mr. Chandan', 'cgupta101@gmail.com', '9779961139', '', 'Snn Raj Seranity yelanahalli, Akshynagar', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-11-28', '09:00', 'Airport', '850', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-27 19:50:00', 'Done', '', 'Offline', 'Chandra', '					', ' \r\n9916690752 Yuvraj   ', 'Manjunath B S', 'Cash'),
(63, 'GMT-61', 'Mr. Sandeep', 'goelsandeep2639@gmail.com', '9748485685', '', 'Little berries play school, #342, 1st A main Road Kormangala 8th block', 'Nandi Hills, Karnataka, India ', '2020-11-29', '03:00', 'Outstation', '3900', 'SUV (INNOVA)-7+1', '2020-11-28 09:11:22', 'Done', '2020-11-29', 'Offline', 'BHIMURAYA', '					', '', 'Manjunath B S', 'Cash'),
(64, 'GMT-62', 'Mr.Saurav Raj', 'sauravraj90@gmail.com', '8892203753', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Blue bell apartment, errapa reddy layout, manjunath nagar, Hosa road, opp Bosch office ', '2020-11-29', '19:00', 'Airport', '950', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-11-28 20:16:41', 'Done', '', 'Offline', 'Chandra', '					', '', 'Manjunath B S', 'Cash'),
(65, 'GMT-63', 'Ms Padma', 'mm@gmail.com', '9663427499', '', 'Comfort Multispeciality Hospital, SRR Arcade,, Kaggadasapura Main Road, C.V. Raman Nagar, Kaggadasapura, Bengaluru, Karnataka 560093', 'Manipal Hospitals - Old Airport Road Bangalore, 98, HAL Old Airport Rd, Kodihalli, Bengaluru, Karnataka 560017', '2020-12-01', '08:30', 'Local', '450', 'SEDAN(DZIRE ETIOS)-4+1', '2020-11-30 04:31:56', 'Done', '', 'Offline', '', '					', '', 'Manjunath B S', 'Cash'),
(66, 'GMT-64', 'Mr Sanjay Agarwal', 'indumathi@tranquilglobal.com', '9821075125', '9900073594', 'Kempegowda International Airport Road, Karnataka, India,	', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095', '2020-12-02', '20:45', 'Airport', '849', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-01 01:11:32', 'Done', '', 'Offline', '', 'Send Invoice					', '', 'Aishwwarya M', 'Cash'),
(67, 'GMT-65', 'Mr Raghvendra Tripathi', 'indumathi@tranquilglobal.com', '9369341263', '9900073594', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095	', '2020-12-02', '17:15', 'Airport', '849', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-01 01:21:10', 'Done', '', 'Offline', 'Sunil kumar', 'Send Invoice					', '', 'Aishwwarya M', 'Cash'),
(68, 'GMT-66', 'Mr Vishnu', 'vishnu.raghunathan@gmail.com', '9321259231', '9717717877', '1st Floor #639, 14th cross, 7th main, Jp nagar 3rd Phase, Lm - By the People', 'Airport And Back', '2020-12-02', '14:55', 'Airport', '1699', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-01 02:23:24', 'Done', '', 'Offline', 'SRIDHAR', '					', '    ', 'Aishwwarya M', 'Cash'),
(69, 'GMT-67', 'Mr Vishnu', 'vishnu.raghunathan@gmail.com', '9321259231', '9717717877', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '1st Floor #639, 14th cross, 7th main, Jp nagar 3rd Phase, Lm - By the People', '2020-12-02', '17:00', 'Airport', '799', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-01 02:25:33', 'Done', '', 'Offline', 'BHIMURAYA', '					', '  ', 'Aishwwarya M', 'Cash'),
(70, 'GMT-68', 'Mr. Anirudh', 'anirudhkaushik99@gmail.com', '7506543488', '', 'SJR Bren TrilliumRayasandra, Bengaluru, Karnataka 560100', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-02', '02:00', 'Airport', '850', 'HATCHBACK-4+1', '2020-12-01 05:47:50', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(71, 'GMT-69', 'Priya Alti', 'priyaalti@gmail.com', '09900581075', '', 'Chitrakut environs , vibhutipura, near basavanagar bus stop ', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-02', '11:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-01 06:21:36', 'Done', '', 'Online', 'BHIMURAYA', '					', '', 'Online', ''),
(72, 'GMT-70', 'Ms.Swati', 'mm@gmail.com', '8910939828', '', 'Little berries Pre school Kormangala', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-02', '08:30', 'Airport', '700', 'HATCHBACK-4+1', '2020-12-01 08:45:57', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(73, 'GMT-71', 'K Mini', 'krismin2399@gmail.com', '09611716822', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Purva riviera apartment ', '2020-12-02', '19:03', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-02 06:36:05', 'Done', '', 'Online', '', '					', '  N: Anil K R\r\nM: 8310475407\r\nV: KA 44 7635 ETIOS', 'Online', ''),
(74, 'GMT-71', 'K Mini', 'krismin2399@gmail.com', '09611716822', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Purva riviera apartment ', '2020-12-02', '19:03', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-02 06:36:26', 'Done', '', 'Online', '', '					', '', 'Online', ''),
(75, 'GMT-73', 'keer', 'KEERTHIMON1988@GMAIL.COM', '96057712', '', 'Kochi, Kerala, India', 'Kottayam, Kerala, India', '03/12020', '12', 'Outstation', '5501316', 'HATCHBACK-4+1', '2020-12-03 00:19:13', 'Customer Cancelled', '03/12/2020', 'Online', '', '					', '', 'Online', ''),
(76, 'GMT-74', 'Ms. Ayushi Ghosal', 'ananth.anu35@gmail.com', '9108344372', '', 'Little Berries playschool 342, 1st A Main Road, Koramangala 8th Block, Koramangala', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-03', '15:00', 'Airport', '700', 'HATCHBACK-4+1', '2020-12-03 01:22:53', 'Done', '', 'Offline', '', '					', ' Done by W258', 'Manjunath B S', 'Cash'),
(77, 'GMT-75', 'Deepak.H.R', 'deepak.swamy98@gmail.com', '8618094931', '9448852409', 'Goraguntepalya Flyover, Prashanth Nagar, T. Dasarahalli, Bengaluru, Karnataka, India', 'KGF, Karnataka, India', '2020-12-03', '18:00', 'Airport', '2299', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-03 05:10:05', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(78, 'GMT-76', 'Nithin', 'fleetwing007@gmail.com', '9743965941', '7795237969', 'Hosur, Tamil Nadu, India', 'Corporation Colony, Bengaluru, Karnataka, India', '2020-12-03', '19:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-03 06:22:28', 'Taxi Not Available', '', 'Online', '', '					', '', 'Online', ''),
(79, 'GMT-77', 'Mr Lekh prasad', 'lekh.prasad@gmail.com', '9739799452', '', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-05', '14:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-03 20:06:44', 'Done', '', 'Offline', 'SACHIN', '					', '  ', 'Ananth', 'Cash'),
(80, 'GMT-78', 'Mr Vaibhav', 'vaibhav.venu@outlook.com', '9901817440', '', 'Assetz  apartments  27th main 1st Sector, HSR Layout, Bengaluru, Karnataka 560102', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-05', '14:30', 'Airport', '899', 'HATCHBACK-4+1', '2020-12-05 00:13:38', 'Customer Cancelled', '', 'Online', '', '', '  Done by 9886909538 W06 ', 'Online', ''),
(81, 'GMT-79', 'Ms. Piu karmakar', 'piukarmakar2050@gmail.com', '8336989433', '8240049616', '1126, 12, 4th Cross Rd, AECS Layout - D Block, AECS Layout, Brookefield, Bengaluru, Karnataka 560037', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-06', '15:15', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-05 00:38:38', 'Customer Cancelled', '', 'Online', 'Sunil kumar', '					', '  Booking Confirmed', 'Online', ''),
(82, 'GMT-80', 'Mr Malikarujuna', 'pujala.monahar@gmail.com', '9880415941', '9959254829', 'Latha ResidencyManjunatha Layout Main Rd, Manjunatha Layout, Shirdi Sai Nagar, Munnekollal, Bengaluru, Karnataka 560037', 'Bagepalli Toll Plaza and back', '2020-12-05', '13:34', 'Outstation', '3000', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-05 01:06:35', 'Done', '2020-12-05', 'Offline', 'CHANDRA', '					', '', 'Manjunath B S', 'Cash'),
(83, 'GMT-81', 'Mr. Khevender', 'khevender.daga@gmail.com', '8800319898', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Carmelaram, Bengaluru, Karnataka, India', '2020-12-06', '01:15', 'Airport', '850', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-05 01:38:00', 'Done', '', 'Offline', 'BHIMURAYA', '					', ' take non toll route Send Etios only ', 'Manjunath B S', 'Cash'),
(84, 'GMT-82', 'Vinay shukla', 'Vinayshukla1302@gmail.com', '7488798210', '8005009295', '3rd Cross, Kaveri Nagar, Murgesh Pallya, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-06', '12:30', 'Airport', '1399', 'SUV MINI (ERITIGA)-6+1', '2020-12-05 23:51:32', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(85, 'GMT-83', 'Vikash Singh', 'vikashsinghw6@gmail.com', '9513611189', '9513711189', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Hospet, Karnataka, India', '2020-12-07', '17:45', 'Outstation', '0', 'SUV (INNOVA CRYSTA)-7+1', '2020-12-06 01:50:44', 'Done', '', 'Online', 'Ganesh', '					', '  Toll ex ', 'Online', ''),
(86, 'GMT-84', 'Vinod padgar', 'vinod9@gmail.com', '7676323184', '7676323184', 'Vishwapriya Nagar, Begur, Bengaluru, Karnataka, India', 'Mysore, Karnataka, India', '2020-12-07', '21:45', 'Airport', '3099', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-06 03:03:19', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(87, 'GMT-85', 'Abhishek ', 'abhishek1911@gmail.com', '9886499437', '', 'Iblur Village, Bellandur, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-07', '05:00', 'Airport', '849', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-06 03:08:17', 'Done', '', 'Offline', 'ANIL', '					', '', 'Ananth', 'Cash'),
(88, 'GMT-86', 'srinivasa', 'ananth.anu35@gmail.com', '9731312911', '', 'Chikkaballapur, Karnataka', 'Chandra Layout, Bengaluru, Karnataka, India', '2020-12-06', '16:15', 'Airport', '2000', 'HATCHBACK-4+1', '2020-12-06 03:13:39', 'Done', '', 'Offline', 'ANIL', '					', '', 'Ananth', 'Cash'),
(89, 'GMT-87', 'Saravanan Balasubramanyam', 'saravananrb@gmail.com', '9880754251', '', 'Goa Airport Arrivals Terminal, Vasco da Gama, Goa, India', 'Saligao, Goa, India', '2020-12-16', '13:45', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-06 03:19:19', 'Customer Cancelled', '', 'Online', '', 'Plan Canceled ', '', 'Online', ''),
(90, 'GMT-88', 'Navya sri', 'navya.grace07@gmail.com', '8099996544', '', 'Rajiv Gandhi International Airport (HYD), Arrivals, Shamshabad, Gollapalle Khurd, Telangana', 'Kakinada, Andhra Pradesh, India', '2020-12-15', '11:30', 'Airport', '10899', 'SUV (INNOVA)-7+1', '2020-12-06 03:30:39', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(91, 'GMT-89', 'Indrajeet', 'ananth.anu35@gmail.com', '8553408637', '', '1 St Main 1st Cross Kaveri Nagar, Mahadevapura, Bengaluru, 560048', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-08', '06:30', 'Airport', '849', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-06 03:52:46', 'Customer Cancelled', '', 'Offline', 'SRIDHAR', '					', '', 'Ananth', 'Cash'),
(92, 'GMT-90', 'Mr Vinodh Kumar', 'ananth.anu35@gmail.com', '9742976404', '8792773968', 'Kormangala Passport Office', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-07', '16:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-06 10:22:15', 'Done', '', 'Offline', 'SRIDHAR', '					', '  ', 'Manjunath B S', 'Cash'),
(93, 'GMT-91', 'Ms. Ayushi Ghosal', 'ananth.anu35@gmail.com', '9108344372', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '  Little Berries playschool 342, 1st A Main Road, 8th Block, Koramangala', '2020-12-07', '19:30', 'Airport', '700', 'HATCHBACK-4+1', '2020-12-06 21:22:38', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(94, 'GMT-92', 'Mr Sheonath prasad', 'lekh.prasad@gmail.com', '8087473145', '9739799452', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-09', '09:15', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-07 01:45:50', 'Done', '', 'Offline', 'SRIDHAR', '					', '    ', 'Manjunath B S', 'Cash'),
(95, 'GMT-93', '', '', '', '', '', '', '', '', '', '', '', '2020-12-09 11:06:47', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(96, 'GMT-94', '', '', '', '', '', '', '', '', '', '', '', '2020-12-09 11:06:48', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(97, 'GMT-95', '', '', '', '', '', '', '', '', '', '', '', '2020-12-09 11:07:04', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(98, 'GMT-96', '', '', '', '', '', '', '', '', '', '', '', '2020-12-09 11:07:05', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(99, 'GMT-97', 'Mr. Sourav Kundu', 'sourav6.k@gmail.com', '9051089546', '', 'Pavani prestige, saibaba temple road, kundnahalli gate, near family super market', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-11', '06:00', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-09 21:53:13', 'Done', '', 'Offline', 'ANIL', '					', '', 'Manjunath B S', 'Cash'),
(100, 'GMT-98', 'Mr Sanjay Agarwal', 'indumathi@tranquilglobal.com', '9821075125', '9900073594', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-11', '16:00', 'Airport', '890', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-10 01:11:09', 'Done', '', 'Offline', 'Ganesh', '					', '    Collect 890 including toll', 'Manjunath B S', 'Cash'),
(101, 'GMT-99', '', '', '', '', '', '', '', '', '', '', '', '2020-12-10 05:42:21', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(102, 'GMT-100', '', '', '', '', '', '', '', '', '', '', '', '2020-12-10 05:42:22', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(103, 'GMT-101', '', '', '', '', '', '', '', '', '', '', '', '2020-12-10 05:42:28', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(104, 'GMT-102', '', '', '', '', '', '', '', '', '', '', '', '2020-12-10 05:42:29', 'Done', '', 'Online', '', '					', '', 'Online', ''),
(105, 'GMT-103', 'Deepak H L', 'deepaknarayana1981@gmail.com', '917975266905', '919731164451', 'Kolkata, West Bengal, India', 'Hotel Green Park, Ameerpet Road, Leelanagar, Ameerpet, Hyderabad, Telangana, India', '2020-12-16', '06:30', 'Airport', '1399', 'SUV MINI (ERITIGA)-6+1', '2020-12-11 04:01:44', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(106, 'GMT-104', 'Zariminush Kujur', 'Zariminushk@gmail.com', '8134902194', '8134902194', 'Thirumalashettyhally, Bengaluru govt school, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-16', '02:15', 'Airport', '899', 'HATCHBACK-4+1', '2020-12-11 04:20:32', 'Done', '', 'Online', 'MANIGANDAN A', '					', '', 'Online', ''),
(107, 'GMT-105', 'Mr. Nithin Pandey', 'dani.nishant@gmail.com', '9986905966', '', '# 22 Abhaya reddy home 17th cross 30 th main behind hotel inchara near jain hostel jp nagar 6th phase, ,	', 'Kempegowda International Airport Bengaluru', '2020-12-12', '13:45', 'Airport', '750', 'HATCHBACK-4+1', '2020-12-11 06:32:58', 'Done', '', 'Offline', 'SHIVA KUMAR K', '					', '  ', 'Aishwwarya M', 'Cash'),
(108, 'GMT-106', '', '', '', '', '', '', '', '', '', '', '', '2020-12-11 11:27:13', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(109, 'GMT-107', '', '', '', '', '', '', '', '', '', '', '', '2020-12-11 11:27:14', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(110, 'GMT-108', '', '', '', '', '', '', '', '', '', '', '', '2020-12-11 11:27:27', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(111, 'GMT-109', '', '', '', '', '', '', '', '', '', '', '', '2020-12-11 11:27:28', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(112, 'GMT-110', 'Mr Vishnu', 'vishnu.raghunathan@gmail.com', '9321259231', '9717717877', '1st Floor #639, 14th cross, 7th main, Jp nagar 3rd Phase, Lm - By the People', '1st Floor #639, 14th cross, 7th main, Jp nagar 3rd Phase, Lm - By the People', '2020-12-12', '16:30', 'Local', '900', 'SEDAN(DZIRE ETIOS)-4+1', '2020-12-11 23:14:25', 'Driver Not Attended', '', 'Offline', '', 'Driver Came Late Customer Booked Other Taxi And Left the Place 	', '      ', 'Manjunath B S', 'Cash'),
(113, 'GMT-111', 'Mr. Vikram', 'vipkssahu@gmail.com', '9555515215', '', 'Prestige lake side habitate tower 16, varthur ', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-12', '13:45', 'Airport', '850', 'HATCHBACK-4+1', '2020-12-11 23:24:41', 'Done', '2020-12-12', 'Offline', 'SANDEEP', '					', '', 'Manjunath B S', 'Cash'),
(114, 'GMT-112', 'Mr Vishnu', 'vishnu.raghunathan@gmail.com', '9321259231', '', '1st Floor #639, 14th cross, 7th main, Jp nagar 3rd Phase, Lm - By the People', 'Airport And Back', '2020-12-13', '09:30', 'Airport', '1699', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-12 19:22:47', 'Done', '', 'Offline', 'BHIMURAYA', '					', '      ', 'Manjunath B S', 'Cash'),
(115, 'GMT-113', 'Mr. Prateek Agarwal', 'prateekagarwal29@gmail.com', '9910093614', '6361876043', 'SSVR tridax balegere rd Varthur', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-15', '06:30', 'Airport', '850', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-12 22:46:52', 'Done', '', 'Offline', 'BHIMURAYA', '					', '', 'Manjunath B S', 'Cash'),
(116, 'GMT-114', 'Mr Sheonath prasad', 'lekh.prasad@gmail.com', '8087473145', '9739799452', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', '2020-12-14', '08:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-14 00:45:55', 'Customer Cancelled', '', 'Offline', '', 'Wrong Booking', '        ', 'Manjunath B S', 'Cash'),
(117, 'GMT-115', 'Mr Sheonath prasad', 'lekh.prasad@gmail.com', '8087473145', '9739799452', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', '2020-12-14', '20:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-14 00:49:31', 'Done', '', 'Offline', 'SANDEEP', '					', '          ', 'Manjunath B S', 'Cash'),
(118, 'GMT-116', 'Alok Raj', 'alokraj44@gmail.com', '9699964448', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'SNN Raj Eterniia, Sai Meadows, Parappana Agrahara, Bengaluru, Karnataka', '2020-12-14', '20:30', 'Airport', '999', 'HATCHBACK-4+1', '2020-12-14 05:17:52', 'Customer Cancelled', '', 'Online', 'BHIMURAYA', '					', '', 'Online', ''),
(119, 'GMT-117', 'Ms. Shilpa', 'shilpaanilsingh@gmail.com', '9902137273', '9845503787', 'Vidyaranyapura, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-15', '05:15', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-14 06:55:17', 'Done', '', 'Online', 'Ganesh', '					', '  ', 'Online', ''),
(120, 'GMT-118', 'Mr. Akansha', '123@gmail.com', '8095337341', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Doddanekundi, Mahadevapura, Bengaluru, Karnataka, India', '2020-12-14', '19:30', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-14 07:07:40', 'Done', '', 'Offline', '', '					', '  Other Cabs', 'Manjunath B S', 'Cash'),
(121, 'GMT-119', 'MR. RAJNEESH KUMAR', 'rajneeshkumar1502@gmail.com', '9608845079', '9430216203', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Kempegowda, Majestic, Bengaluru, Karnataka, India', '2020-12-14', '19:55', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-14 07:21:24', 'Done', '', 'Online', '', '					', '   KA 03 AD 3906  9738699359', 'Online', ''),
(122, 'GMT-120', 'Mr Ankith Kejriwal', 'ankith88kej@gmail.com', '9599586731', '', 'Sobha Dream Acres Panathur Main Road, Off, Outer Ring Rd, Balagere, Bengaluru, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-15', '07:15', 'Airport', '750', 'HATCHBACK-4+1', '2020-12-14 07:50:55', 'Done', '', 'Offline', 'SRIDHAR', '					', 'Take non Toll route take only 750', 'Manjunath B S', 'Cash'),
(123, 'GMT-121', 'Balbir Kumar', 'balbir4usingh@gmail.com', '08861921066', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Vignana Nagar, Doddanekundi, Doddanekkundi, Bengaluru, Karnataka, India', '2020-12-15', '11:39', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-14 22:39:28', 'Done', '', 'Online', 'ANIL', '					', '', 'Online', ''),
(124, 'GMT-122', 'Mr. J Vasudev', 'm@gmail.com', '9481479696', '', 'Sri Sai Lake view Apartment, Varthur road Opp star market', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-19', '07:00', 'Airport', '750', 'HATCHBACK-4+1', '2020-12-15 22:35:18', 'Done', '', 'Offline', 'ANIL', '					', '', 'Manjunath B S', 'Cash'),
(125, 'GMT-123', 'Naveen', 'naveen9234@gmail.com', '853136699', '', 'Marathahalli, Bengaluru, Karnataka, India', 'Kanakapura, Karnataka, India', '2020-12-25', '09:31', 'Outstation', '1950', 'HATCHBACK-4+1', '2020-12-16 11:22:50', 'Customer Cancelled', '2020-12-26', 'Online', '', '					', '', 'Online', ''),
(126, 'GMT-124', 'Mr. Karthik', 'aa@gmail.com', '9036442121', '', 'Rajaji Nagar, Bengaluru, Karnataka, India,	', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-19', '03:00', 'Airport', '700', 'HATCHBACK-4+1', '2020-12-18 01:00:34', 'Done', '', 'Offline', 'ANIL', '					', '', 'Manjunath B S', 'Cash'),
(127, 'GMT-125', 'Mr Nanditha Sao', 'snanda16182@gmail.com', '9555077757', '', 'Pristige lakeside habitat, Varthur, bangalore', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-19', '09:00', 'Airport', '850', 'HATCHBACK-4+1', '2020-12-18 04:10:43', 'Done', '', 'Offline', 'MANIGANDAN A', '					', '', 'Manjunath B S', 'Cash'),
(128, 'GMT-126', 'Piu karmakar', 'piukarmakar2050@gmail.com', '8336989433', '7992318195', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Kundalahalli, Brookefield, Bengaluru, Karnataka, India', '2020-12-20', '22:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-19 23:06:21', 'Done', '', 'Online', '', '					', '', 'Online', ''),
(129, 'GMT-126', 'Piu karmakar', 'piukarmakar2050@gmail.com', '8336989433', '7992318195', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Kundalahalli, Brookefield, Bengaluru, Karnataka, India', '2020-12-20', '22:00', 'Airport', '799', 'HATCHBACK-4+1', '2020-12-19 23:06:22', 'Done', '', 'Online', 'HARISH', '					', '', 'Online', ''),
(130, 'GMT-128', 'Mr. Karthik', 'aa@gmail.com', '9036442121', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Rajaji Nagar, Bengaluru, Karnataka, India', '2020-12-21', '12:45', 'Airport', '700', 'HATCHBACK-4+1', '2020-12-20 21:29:09', 'Done', '', 'Offline', 'Sunil kumar', '					', '  ', 'Manjunath B S', 'Cash'),
(131, 'GMT-129', 'Mr. Nithin Pandey', 'dani.nishant@gmail.com', '9986905966', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '# 22 Abhaya reddy home 17th cross 30 th main behind hotel inchara near jain hostel jp nagar 6th phase', '2020-12-24', '18:00', 'Airport', '750', 'HATCHBACK-4+1', '2020-12-22 00:22:48', 'Customer Cancelled', '', 'Offline', '', '					', '    ', 'Manjunath B S', 'Cash'),
(132, 'GMT-130', 'Ms. Amit Agarwal', 'amit.april04@gmail.com', '9960263242', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '# 22 Abhaya reddy home 17th cross 30 th main behind hotel inchara near jain hostel jp nagar 6th phase,', '2020-12-24', '18:00', 'Airport', '750', 'HATCHBACK-4+1', '2020-12-22 00:30:22', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(133, 'GMT-131', 'Mr Vishnu', 'vishnu.raghunathan@gmail.com', '9321259231', '9717717877', '1st Floor #639, 14th cross, 7th main, Jp nagar 3rd Phase, Lm - By the People', 'Chennai, Tamil Nadu, India ', '2020-12-26', '04:45', 'Outstation', '10', 'SEDAN (DZIRE, ETIOS)-4+1', '2020-12-23 23:11:11', 'Done', '2020-12-27', 'Offline', 'MANIGANDAN A', '					', '              ', 'Manjunath B S', 'Cash'),
(134, 'GMT-132', 'Mr Lekh prasad', 'lekh.prasad@gmail.com', '9739799452', '8087473145', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Railway Station Road, Kempegowda, Majestic, Bengaluru, Karnataka', '2020-12-25', '17:45', 'Local', '800', 'SEDAN(DZIRE ETIOS)-4+1', '2020-12-24 23:08:27', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '      ', 'Manjunath B S', 'Cash'),
(135, 'GMT-133', 'Mr. Gaurav ', 'ashish.mantri@yahoo.com', '7411051535', '9886585565', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Reliance trend Rajarajeshwari nagar ', '2020-12-26', '13:30', 'Airport', '1599', 'SUV (INNOVA)-7+1', '2020-12-25 03:57:20', 'Done', '', 'Offline', '', '					', ' Take non toll route  Done by other driver', 'Manjunath B S', 'Cash'),
(136, 'GMT-134', 'Mr Ajay', 'ajay.goyal1304@gmail.com', '8800281444', '', 'Ahad Euphoria Road, Carmelaram, Hadosiddapura, Chikkakannalli, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-26', '12:00', 'Airport', '800', 'HATCHBACK-4+1', '2020-12-25 21:06:47', 'Done', '', 'Offline', '', '					', '   Take non toll route  Ragavendra KA 03 AA 2449 9900176418', 'Manjunath B S', 'Cash'),
(137, 'GMT-135', 'Mr Sandesh', 'gmail@gmail.com', '8105562171', '', 'Vidyaranyapura post office', 'Tumkur and back', '2020-12-27', '08:00', 'Outstation', '12', 'SUV MINI (ERITIGA)-6+1', '2020-12-26 05:52:50', 'Done', '2020-12-27', 'Offline', 'SRIDHAR', '					', '', 'Aishwwarya M', 'Cash'),
(138, 'GMT-136', 'Mr. SAURAB ', 'saurabhjais11@gmail.com', '8892766448', '', 'Blue Bell Apartment, Hosur road electronic city', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2020-12-28', '01:00', 'Airport', '899', 'HATCHBACK-4+1', '2020-12-27 06:57:46', 'Done', '', 'Offline', 'MANIGANDAN A', '					', '', 'Manjunath B S', 'Cash'),
(139, 'GMT-137', 'Ms. Ayushi Ghosal', 'ananth.anu35@gmail.com', '9108344372', '9748485685', 'Little Berries playschool 342, 1st A Main Road, Koramangala 8th Block, Koramangala', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-01-04', '13:00', 'Airport', '700', 'HATCHBACK-4+1', '2021-01-03 03:45:48', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(140, 'GMT-138', 'Mr. B Das', 'nummy451@yahoo.co.in', '9980833352', '', 'Manu Residency, Kadugodi Bangalore', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-01-08', '08:30', 'Airport', '799', 'HATCHBACK-4+1', '2021-01-04 21:54:13', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', 'Take non toll route', 'Manjunath B S', 'Cash'),
(141, 'GMT-139', 'Mr. Ganesh', 'ananth@gmail.com', '6362327895', '', 'Ejipura, Bengaluru, Karnataka', 'Kempegowda International Airport and Back', '2021-01-10', '06:00', 'Airport', '1500', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-01-08 23:30:21', 'Taxi Not Available', '', 'Offline', 'AJAY KUMAR HS', '					', '', 'Manjunath B S', 'Cash');
INSERT INTO `booking` (`id`, `bid`, `name`, `email`, `number`, `anumber`, `pickup`, `drop`, `date`, `time`, `package`, `price`, `tc`, `bdt`, `status`, `edate`, `btype`, `driver`, `feedback`, `instruction`, `book_by`, `ptype`) VALUES
(142, 'GMT-140', 'Ms. Priyanka', 'ankush@gmail.com', '8199919922', '7206566760', '24, 20 F Cross Road, Ashwini Layout, Ejipura, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-01-10', '03:15', 'Airport', '799', 'HATCHBACK-4+1', '2021-01-08 23:46:55', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '  ', 'Manjunath B S', 'Cash'),
(143, 'GMT-141', 'Mr. Anupam Sharma', 'indumathi@tranquilglobal.com', '8368179825', '9900073594', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095', '2021-01-10', '11:00', 'Airport', '0', 'HATCHBACK-4+1', '2021-01-09 02:33:25', 'Done', '', 'Offline', 'SRIDHAR', '					', '  Credit Duty. Billing will be 799+95', 'Manjunath B S', 'Credit'),
(144, 'GMT-142', 'Mr Vikram', 'vickssahu@gmail.com', '9310315301', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Prestige Lakeside Habitat vartur', '2021-01-09', '20:00', 'Airport', '849', 'HATCHBACK-4+1', '2021-01-09 03:03:38', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '  ', 'Aishwwarya M', 'Cash'),
(145, 'GMT-143', 'Ms. Bipin Chandra ', 'sanbip.mahopatra@gmail.com', '9437693646', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Prestige lakeside habitat Varthur', '2021-01-12', '11:30', 'Airport', '799', 'HATCHBACK-4+1', '2021-01-11 08:59:52', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(146, 'GMT-144', 'Mr. Vaibhav', 'vaibhav.arora85@gmail.com', '9650834155', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Suncity Gloria Apartment Sarjapur road', '2021-01-13', '22:15', 'Airport', '899', 'HATCHBACK-4+1', '2021-01-13 02:39:14', 'Customer Cancelled', '', 'Offline', '', '					', '', 'Manjunath B S', 'Cash'),
(147, 'GMT-145', 'Ms. Guest', 'm@gmail.com', '8260023749', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Alliance University Anakel bangalore', '2021-01-14', '15:00', 'Airport', '1300', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-01-13 08:54:45', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '', 'Manjunath B S', 'Cash'),
(148, 'GMT-146', 'Roni Sarkar', 'ronisarkar123@yahoo.co.in', '07022509309', '', 'Balagere Road, Devasthanagalu, Varthur, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-01-16', '19:00', 'Airport', '899', 'HATCHBACK-4+1', '2021-01-14 03:45:01', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(149, 'GMT-147', 'Roni Sarkar', 'ronisarkar123@yahoo.co.in', '07022509309', '', 'Balagere Road, Devasthanagalu, Varthur, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-01-16', '06:00', 'Airport', '899', 'HATCHBACK-4+1', '2021-01-14 04:47:43', 'Done', '', 'Online', 'ANIL', '					', '   Take non toll route  ', 'Online', ''),
(150, 'GMT-148', 'Mr. Thooran Malik', 'm@gmail.com', '8971791272', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Manjusri Luxuria, Old Madiwala, Madiwala, 1st Stage, BTM Layout, Bengaluru', '2021-01-22', '23:15', 'Airport', '799', 'HATCHBACK-4+1', '2021-01-21 22:23:58', 'Done', '', 'Offline', '', '					', '', 'Manjunath B S', 'Cash'),
(151, 'GMT-149', 'Mr. Sridhar', 'm@gmail.com', '9986651630', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Gandhinagar, Bengaluru, Karnataka, India', '2021-01-25', '15:00', 'Airport', '700', 'HATCHBACK-4+1', '2021-01-24 23:35:25', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(152, 'GMT-150', 'Ms.Swati', 'swatimohapatra3@gmail.com', '9439481007', '', 'Prestige lakeside habitat Varthur', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-02-02', '02:45', 'Airport', '850', 'HATCHBACK-4+1', '2021-01-31 20:04:09', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '', 'Manjunath B S', 'Cash'),
(153, 'GMT-151', '', '', '', '', '', '', '', '', '', '', '', '2021-02-01 13:00:37', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(154, 'GMT-152', 'Mr. Raghaba bhasker patro', 'raghabapatro@yahoo.com', '70019187120', '', 'Prestige Lakeside habitat Varthur', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-02-06', '02:00', 'Airport', '950', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-02-03 22:09:51', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '', 'Manjunath B S', 'Cash'),
(155, 'GMT-153', 'Mr Lekh prasad', 'lekh.prasad@gmail.com', '9739799452', '', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-02-13', '05:30', 'Airport', '799', 'HATCHBACK-4+1', '2021-02-12 02:25:53', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '    ', 'Manjunath B S', 'Cash'),
(156, 'GMT-154', 'raju', 'admin@gmail.com', '7894561230', '9966332211', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Vijayawada, Andhra Pradesh, India', '2021-02-28', '20:43', 'Airport', '13199', 'HATCHBACK-4+1', '2021-02-13 03:08:51', 'Customer Not Attended', '', 'Online', '', '					', '', 'Online', ''),
(157, 'GMT-155', 'manjunath', 'manjunath.23.6@gmail.com', '7406001992', '', 'BTM Layout, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-02-13', '16:57', 'Airport', '799', 'HATCHBACK-4+1', '2021-02-13 03:27:50', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(158, 'GMT-156', 'Mr. Saurabh Chawla', 'indumathi@tranquilglobal.com', '8368179825', '9560905393', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095', '2021-02-22', '09:45', 'Airport', '0', 'HATCHBACK-4+1', '2021-02-20 00:52:35', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '799 plus 95 billing', 'Manjunath B S', 'Credit'),
(159, 'GMT-157', 'Mr. Saurabh Chawla', 'indumathi@tranquilglobal.com', '8368179825', '9560905393', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-02-22', '16:00', 'Airport', '0', 'HATCHBACK-4+1', '2021-02-20 00:54:24', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', ' 799 plus 95 Credit billing ', 'Manjunath B S', 'Credit'),
(160, 'GMT-158', 'Ms. Ayushi Ghosal', 'ananth.anu35@gmail.com', '9108344372', '', 'Little Berries playschool 342, 1st A Main Road, 8th Block, Koramangala', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-02-22', '05:00', 'Airport', '700', 'HATCHBACK-4+1', '2021-02-21 03:36:34', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '  ', 'Manjunath B S', 'Cash'),
(161, 'GMT-159', 'Mr. Raghaba bhasker patro', 'raghabapatro@yahoo.com', '7001987120', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Prestige lakeside habitat Varthur', '2021-02-22', '15:15', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-02-21 05:04:34', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(162, 'GMT-160', 'Mr. Ranjit Kumar Sahu', 'sahuranjit1952@gmail.com', '9338692074', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Prestige Lakeside habitat Varthur', '2021-02-27', '21:00', 'Airport', '850', 'HATCHBACK-4+1', '2021-02-26 21:25:01', 'Done', '', 'Offline', '', '					', '', 'Manjunath B S', 'Cash'),
(163, 'GMT-161', 'Mr. Raghaba bhasker patro', 'raghabapatro@yahoo.com', '7001987120', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Prestige lakeside habitat Varthur', '2021-03-01', '15:15', 'Airport', '800', 'HATCHBACK-4+1', '2021-02-28 21:05:38', 'Done', '', 'Offline', 'CHANDRA', '					', '      Take non toll route', 'Manjunath B S', 'Cash'),
(164, 'GMT-162', 'Mr. Indrani', 'indranichoudhury1412@gmail.com', '9738277845', '7349068840', 'NTR Layout, Maruthi Nilaya, Chowdeswari temple Street 2nd right cross, New Age School Road, Marathalli', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-03-03', '03:30', 'Airport', '799', 'HATCHBACK-4+1', '2021-03-02 09:43:35', 'Done', '', 'Offline', 'ANIL', '					', '', 'Manjunath B S', 'Cash'),
(165, 'GMT-163', 'Mr. Lokanath Baishya', 'lokanath077@gmail.com', '8884711761', '', 'Gym Square - F2 Group, Munnekollal Main Road, Near Govt School', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-03-05', '14:30', 'Airport', '799', 'HATCHBACK-4+1', '2021-03-03 06:33:05', 'Done', '', 'Offline', '', '					', '', 'Manjunath B S', 'Cash'),
(166, 'GMT-164', 'Mr Mukesh Kumar', 'indumathi@tranquilglobal.com', '9900073594', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Fab Hotel 81/D, 2nd Cross Rd, Opp Aero Fitness Gym, KHB Colony, 5th Block, Koramangala, Bengaluru, Karnataka 560095', '2021-03-09', '12:30', 'Airport', '0', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-08 03:25:40', 'Done', '', 'Offline', '', '					', '    ', 'Aishwwarya M', 'Credit'),
(167, 'GMT-165', 'Ms Ayushi Roy', 'indumathi@tranquilglobal.com', '9369341263', '9503079038', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Fab Hotel 81/D, 2nd Cross Rd, Opp Aero Fitness Gym, KHB Colony, 5th Block, Koramangala, Bengaluru, Karnataka 560095', '2021-03-09', '17:35', 'Airport', '0', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-08 03:27:26', 'Done', '', 'Offline', '', '					', '      ', 'Aishwwarya M', 'Credit'),
(168, 'GMT-166', 'Mr Mihir Sudhir Sawant', 'indumathi@tranquilglobal.com', '9900073594', '9167400867', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Fab Hotel 81/D, 2nd Cross Rd, Opp Aero Fitness Gym, KHB Colony, 5th Block, Koramangala, Bengaluru, Karnataka 560095', '2021-03-09', '19:40', 'Airport', '0', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-08 03:30:07', 'Done', '', 'Offline', 'SRIDHAR', '					', '      ', 'Aishwwarya M', 'Credit'),
(169, 'GMT-167', 'Mr Mukesh Kumar', 'indumathi@tranquilglobal.com', '9900073594', '', 'Fab Hotel 81/D, 2nd Cross Rd, Opp Aero Fitness Gym, KHB Colony, 5th Block, Koramangala, Bengaluru, Karnataka 560095', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-03-18', '14:45', 'Airport', '0', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-17 20:02:51', 'Done', '', 'Offline', 'SRIDHAR', '					', '      ', 'Manjunath B S', 'Credit'),
(170, 'GMT-168', 'Ms. Shilpa', 'shilpaanilsingh@gmail.com', '9902137273', '9845503787', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Vasanthpura, ISRO Layout, Bengaluru, Karnataka, India', '2021-03-19', '02:15', 'Airport', '799', 'HATCHBACK-4+1', '2021-03-18 01:45:30', 'Done', '', 'Offline', '', '					', '    ', 'Manjunath B S', 'Cash'),
(171, 'GMT-169', 'Mr Sanjay Agarwal', 'indumathi@tranquilglobal.com', '9900073594', '9902597354', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095	', '2021-03-26', '10:00', 'Airport', '0', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-25 07:10:07', 'Done', '', 'Offline', 'SRIDHAR', '					', '  ', 'Manjunath B S', 'Credit'),
(172, 'GMT-170', 'Mr Anand Mishra', 'indumathi@tranquilglobal.com', '9900073594', '9741469032', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '60th Feet Road, KHB Block Koramangala, 5th Block, Koramangala, Bangalore, Karnataka 560095, India', '2021-03-26', '09:15', 'Airport', '0', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-25 07:15:28', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '    ', 'Manjunath B S', 'Credit'),
(173, 'GMT-171', 'Mr Anand Mishra and Mr Sanjay Agarwal', 'indumathi@tranquilglobal.com', '9900073594', '9741469032', '60th Feet Road, KHB Block Koramangala, 5th Block, Koramangala, Bangalore, Karnataka 560095, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-03-27', '16:00', 'Airport', '0', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-26 05:03:49', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '      ', 'Manjunath B S', 'Credit'),
(174, 'GMT-172', 'Mr. B Das', 'nummy451@yahoo.co.in', '9980833352', '', 'F302, sriram spurithi near CMRIT COLLEGE, Brookfield, Kundanahalli bangalore 560037', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-03-31', '13:15', 'Airport', '899', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-29 21:36:58', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '   Take non toll route   ', 'Aishwwarya M', 'Cash'),
(175, 'GMT-173', 'Mr. Rahual', 'manjunath@gmail.com', '8802397566', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Outer Ring Road, BDA Layout, Chandra Layout, Nagarbhavi, Bengaluru, Karnataka, India', '2021-03-31', '18:39', 'Airport', '800', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-03-31 06:09:57', 'Done', '', 'Offline', 'AJAY KUMAR HS', '					', '', 'Manjunath B S', 'Cash'),
(176, 'GMT-174', 'Mr Tuhin Das', 'indumathi@tranquilglobal.com', '9900073594', '9038044887', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095	', '2021-04-06', '19:00', 'Airport', '0', 'HATCHBACK-4+1', '2021-04-05 22:08:24', 'Done', '', 'Offline', '', '					', '        ', 'Manjunath B S', 'Cash'),
(177, 'GMT-175', 'Mr Tuhin Das', 'indumathi@tranquilglobal.com', '9900073594', '9038044887', '#34, 4th floor, 60th feet road, 5th cross, 5th block, Koramangala Bangalore 560095	', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-04-15', '17:00', 'Airport', '0', 'HATCHBACK-4+1', '2021-04-14 06:06:28', 'Pending', '', 'Offline', '', '', '          ', 'Manjunath B S', 'Credit'),
(178, 'GMT-176', 'Manjunath', 'manjunath.23.6.1991@gmail.com', '9980037978', '', 'Electronic City Phase II, Electronic City, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-04-16', '06:00', 'Airport', '999', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-04-15 05:24:58', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(179, 'GMT-177', '', '', '', '', '', '', '', '', '', '', '', '2021-04-26 05:31:09', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(180, 'GMT-178', 'Mr. Sourabh Kumar', 'sourabhkumar937@gmail.com', '7702352032', '', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-05-01', '11:00', 'Airport', '999', 'SEDAN (DZIRE, ETIOS)-4+1', '2021-04-30 00:23:36', 'Done', '', 'Offline', 'SRIDHAR', '					', '', 'Manjunath B S', 'Cash'),
(181, 'GMT-179', 'Mr. Dinesh Chouhan', 'm@gmail.com', '9448193144', '', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', 'Puttenalhalli JP Nagar 7th Phase', '2021-05-01', '01:00', 'Airport', '1599', 'SUV (INNOVA CRYSTA)-7+1', '2021-04-30 05:01:28', 'Customer Cancelled', '', 'Offline', '', '					', '', 'Manjunath B S', 'Cash'),
(182, 'GMT-180', '', '', '', '', '', '', '', '', '', '', '', '2021-05-09 18:33:37', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(183, 'GMT-181', '', '', '', '', '', '', '', '', '', '', '', '2021-05-09 18:33:39', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(184, 'GMT-182', '', '', '', '', '', '', '', '', '', '', '', '2021-05-09 18:33:54', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(185, 'GMT-183', '', '', '', '', '', '', '', '', '', '', '', '2021-05-09 18:33:55', 'Customer Cancelled', '', 'Online', '', '					', '', 'Online', ''),
(186, 'GMT-184', 'Mr. Kumar Ranjan', 'kumar.ranjan@live.com', '7411251112', '', '30, 12th Main Road, KEB Colony, 1st Stage, BTM 1st Stage, Bengaluru, Karnataka, India', 'Kempegowda International Airport Road, Yerthiganahalli, Bengaluru Rural, Karnataka, India', '2021-05-14', '10:30', 'Airport', '899', 'SEDAN (ETIOS)-4+1', '2021-05-13 05:11:29', 'Done', '', 'Online', 'SRIDHAR', '					', '  ', 'Online', ''),
(187, 'GMT-185', 'Mr  Varun', 'varun.sood@yahoo.com', '7022277279', '9779664322', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-05-22', '02:00', 'Airport', '999', 'SEDAN (ETIOS)-4+1', '2021-05-18 06:49:26', 'Customer Cancelled', '', 'Offline', '', '					', '  ', 'Manjunath B S', 'Cash'),
(188, 'GMT-186', 'Mr  Varun', 'varun.sood@yahoo.com', '7022277279', '9779664322', 'Samruddhi Homes - Uplands, Devasthanagalu, Varthur, Karnataka 560087', 'Kempegowda International Airport Road, Yerthiganahalli, Karnataka, India', '2021-05-29', '02:00', 'Airport', '999', 'SEDAN (ETIOS)-4+1', '2021-05-27 05:06:28', 'Pending', '', 'Offline', '', '', '    ', 'Manjunath B S', 'Cash'),
(197, '186', 'Vikash', 'vy8412@gmail.com', '8319278218', NULL, 'Bangalore, Karnataka, India', 'Hyderabad, Telangana, India', '2021-06-05', '10:00', 'Airport', '6290', 'SEDAN (DZIRE, ETIOS) - 4+1', '2021-06-05 03:31:22', 'Pending', '2021-06-06', 'Online', NULL, NULL, NULL, 'Online', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dr`
--

CREATE TABLE `dr` (
  `id` int(255) NOT NULL,
  `bid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `pickup` varchar(255) NOT NULL,
  `drop` varchar(255) NOT NULL,
  `pdate` varchar(255) NOT NULL,
  `cnumber` varchar(255) NOT NULL,
  `ctype` varchar(255) NOT NULL,
  `dname` varchar(255) NOT NULL,
  `okms` varchar(255) NOT NULL,
  `ckms` varchar(255) NOT NULL,
  `otime` varchar(255) NOT NULL,
  `ctime` varchar(255) NOT NULL,
  `tr` varchar(255) NOT NULL,
  `sign` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dr`
--

INSERT INTO `dr` (`id`, `bid`, `name`, `number`, `pickup`, `drop`, `pdate`, `cnumber`, `ctype`, `dname`, `okms`, `ckms`, `otime`, `ctime`, `tr`, `sign`, `date`) VALUES
(1, 'Gmt-1', 'Manjunath', '7406001991', '#54, 4th Cross, V P Road', 'Airport', '2021-04-16', 'KA 03 AA 1234', 'INNOVA', 'Manju', '100', '200', '18:00', '20:00', 'IMG20210415180022.jpg,', '6078320449bc4.png', '2021-04-15 05:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(255) NOT NULL,
  `did` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `vn` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `did`, `name`, `number`, `vn`, `date`, `status`) VALUES
(4, 'G1', 'Vikash Yadav', '8319278218', 'BR 29 A 8516', '31-Oct-2020', 'Deactive'),
(5, 'G2', 'SRIDHAR', '9986651630', 'KA 51 AF 2941 DZIRE', '14-Nov-2020', 'Active'),
(6, 'G3', 'CHANDRA', '8970642793', 'KA 42 A 3819 ETIOS', '16-Nov-2020', 'Active'),
(7, 'G4', 'Syed Sadiq', '9663819822', 'KA 50 9423 Innova', '17-Nov-2020', 'Deactive'),
(8, 'G5', 'Sudeep', '8618714915', 'KA 05 AA 5555', '18-Nov-2020', 'Deactive'),
(9, 'G6', 'ANIL', '8971119202', 'KA 04 AC 0693 ETIOS', '19-Nov-2020', 'Active'),
(10, 'G7', 'BHIMURAYA', '8197167585', 'KA 51 AF 3900', '21-Nov-2020', 'Active'),
(11, 'G8', 'Ganesh', '7892875546', 'KA 05 MW 3602', '22-Nov-2020', 'Active'),
(12, 'G9', 'Chandu', '9972204407', 'KA 01 AJ 1847 CRYSTA', '23-Nov-2020', 'Deactive'),
(13, 'G10', 'Sunil kumar', '9740063206', 'KA 05 MW 3602 ETIOS', '24-Nov-2020', 'Active'),
(14, 'G11', 'CHETHAN SM', '9731985020', 'KA 04 AB 0313 CIAZ', '02-Dec-2020', 'Active'),
(15, 'G12', 'PRAVEEN GR', '7338248604', 'KA 51 AF 1588', '02-Dec-2020', 'Active'),
(16, 'G13', 'HARISH', '9902101025', 'KA 05 AG 6375', '05-Dec-2020', 'Active'),
(17, 'G14', 'SANDEEP', '7795442205', 'KA 02 AC 6766', '12-Dec-2020', 'Active'),
(18, 'G15', 'SHIVA KUMAR K', '7892010516', 'KA 41 C 2485', '12-Dec-2020', 'Active'),
(19, 'G16', 'KISHORE KUMAR K', '9986746999', 'KA 05 AK 3167', '14-Dec-2020', 'Active'),
(20, 'G17', 'MANIGANDAN A', '9986681380', 'KA 01 AJ 2236', '15-Dec-2020', 'Active'),
(21, 'G18', 'THEJAS JN', '7337612845', 'KA 04 AB 8031', '15-Dec-2020', 'Active'),
(22, 'G19', 'NAVEEN KUMAR M', '6360278755', 'KA 41 B 6010', '15-Dec-2020', 'Active'),
(23, 'G20', 'AJAY KUMAR HS', '7892785550', 'KA 51 AF 2941 DZIRE', '25-Dec-2020', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`id`, `name`, `email`, `number`, `message`, `date`) VALUES
(23, 'Melba Dominique', 'melba@bestlocaldata.com', '02681 17 65 70', 'Hello from BestLocalData.com\r\n\r\nDue to the pandemic BestLocalData.com is shutting down on the 14th of May.\r\n\r\nWe have more than a 100 million records of Key Executives all over the world.\r\n\r\nWe hope that this Data will serve other companies to succeed in their marketing efforts.\r\n\r\nWe have reduced all the prices to next to nothing on our website BestLocalData.com\r\n\r\nWe wish you the best in your future endeavours.', '11-May-2021'),
(24, 'Carina Pinedo', 'pinedo.carina@outlook.com', '(67) 3392-8633', 'Buy qualified web traffic for your site. Keyword Targeted. Full refund if not happy.  http://bit.ly/boost-web-traffic-now', '14-May-2021'),
(25, 'Tamera', 'info@getmytaxi.in', '956 33 719', 'Good day\r\n\r\nBuy face mask to protect your loved ones from the deadly CoronaVirus.  We wholesale N95 Masks and Surgical Masks for both adult and kids.  The prices begin at $0.19 each.  If interested, please check our site: pharmacyoutlets.online\r\n\r\nKind Regards,\r\n\r\nTamera\r\nAirport Taxi in Bangalore,Airport Taxi near me,Airport Taxi in Karnataka.', '16-May-2021'),
(26, 'Carmel Lewis', 'carmel@order-fulfillment.net', '078 1125 7874', 'Hello from order-fulfillment.net,\r\n\r\nDoing your own product shipping or order fulfillment in house?\r\n\r\nTired of it? Visit us on www.order-fulfillment.net  \r\n\r\nWe can store, inventory, and manage your drop shipping / order fulfillment for you.  \r\n\r\nBased in the US for almost 2 decades - we ship around the world and will save you time and money.\r\n\r\nWho would be the best contact at your company to discuss?\r\n\r\nHere are some of the items we ship for clients:\r\n-Books, training manuals, guides\r\n-New member welcomes boxes and gifts\r\n-Product samples\r\n-Marketing materials\r\n-Medical program test kits\r\n-Follow up gifts to clients, leads, and prospects\r\n\r\nThank you!\r\nFulfillment Warehouse\r\nhttps://order-fulfillment.net', '18-May-2021'),
(27, 'Amanda Roden', 'amanda@fbcourses.net', '02638 41 80 15', 'Are you looking for the best way to market your business?\r\n\r\nFbCourses.net has the answer..\r\n\r\nNot sure where to start, or what to do?\r\n\r\nWhy not learn from the best.  All of the best.\r\n\r\nWe are offering at never before seen prices all of the top courses for one insanely low price.\r\n\r\nIf you have been thinking of how to generate more leads, website traffic or sales today is the day.\r\n\r\nAll courses are available in full with immediate download on FbCourses.net\r\n\r\nNot sure which one you like, for the next 24 hours we are offering the entire suite of courses for the lowest amount you could possible imagine.\r\n\r\nCheck us out : FbCourses.net\r\n\r\nYou could be driving new income or starting the new side hustle today!', '19-May-2021'),
(28, 'Arnulfo', 'info@getmytaxi.in', '0461 93 69 57', 'Hey\r\n\r\nBe Buzz Free! The Original Mosquito Trap.\r\n\r\n60% OFF for the next 24 Hours ONLY + FREE Worldwide Shipping\r\nâœ”ï¸LED Bionic Wave Technology\r\nâœ”ï¸Eco-Friendly\r\nâœ”ï¸15 Day Money-Back Guarantee\r\n\r\nShop Now: mosquitotrap.online\r\n\r\nThanks and Best Regards,\r\n\r\nArnulfo\r\nAirport Taxi in Bangalore,Airport Taxi near me,Airport Taxi in Karnataka.', '22-May-2021'),
(29, 'Marcelino Luce', 'marcelino@bestlocaldata.com', '(21) 2532-5134', 'Hello,\r\n\r\nIt is with sad regret to inform you that BestLocalData.com is shutting down.\r\n\r\nWe have made all our databases for sale for a once-off price.\r\n\r\nVisit our website to get the best bargain of your life. BestLocalData.com\r\n\r\nRegards,\r\nMarcelino', '25-May-2021'),
(30, 'Philomena', 'info@getmytaxi.in', '(02) 6141 9522', 'Hey there\r\n\r\nCAREDOGBESTâ„¢ - Personalized Dog Harness. All sizes from XS to XXL.  Easy ON/OFF in just 2 seconds.  LIFETIME WARRANTY.\r\n\r\nClick here: caredogbest.online\r\n\r\nBest,\r\n\r\nPhilomena\r\nAirport Taxi in Bangalore,Airport Taxi near me,Airport Taxi in Karnataka.', '25-May-2021'),
(31, 'Vada', 'vada@getmytaxi.in', '306-538-0050', 'Hi \r\n \r\nProviding Premium sunglasses only $19.99 for the next 24 Hours ONLY.  60% OFF today with free worldwide shipping.\r\n\r\nGet Yours: trendshades.online\r\n \r\nSincerely, \r\n \r\nVada\r\nAirport Taxi in Bangalore,Airport Taxi near me,Airport Taxi in Karnataka.', '01-Jun-2021'),
(32, 'Korey Fitzwater', 'korey@bestlocaldata.com', '0911-9849591', 'Hello,\r\n\r\nIt is with sad regret to inform you that BestLocalData.com is shutting down.\r\n\r\nWe have made all our databases for sale for a once-off price.\r\n\r\nVisit our website to get the best bargain of your life. BestLocalData.com\r\n\r\nRegards,\r\nKorey', '01-Jun-2021');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flasher`
--

CREATE TABLE `flasher` (
  `sl.no` int(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `up_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flasher`
--

INSERT INTO `flasher` (`sl.no`, `image`, `status`, `up_date`) VALUES
(1, 'Untitled.png', 'Hide', '2020-11-07 03:42:44');

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `id` int(255) NOT NULL,
  `key_page` varchar(255) NOT NULL,
  `key_slug` varchar(255) NOT NULL,
  `key_title` varchar(255) NOT NULL,
  `key_desc` varchar(255) NOT NULL,
  `key_words` varchar(255) NOT NULL,
  `key_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `key_page`, `key_slug`, `key_title`, `key_desc`, `key_words`, `key_date`) VALUES
(1, 'Home | Airport Taxi', 'index', 'Airport Taxi in Bangalore,Airport Taxi near me,Airport Taxi in Karnataka.', 'Book Taxi from Airport to anywhere in Bangalore at just Rs.799/-, Call - +918884488355.  Airport Taxi in Bengaluru, Airport Cabs Bangalore, Airport Taxi Service Bangalore,  Airport Taxi, Airport,Cabs.', 'Airport Taxi in Bangalore,Airport Taxi near me,Airport Taxi in Karnataka,Airport Taxi,Airport Taxi in India.', '2020-11-28 23:40:11'),
(2, 'Local Taxi', 'Local', 'Local Taxi service in Bangalore | Book cab at affordable price.', 'Find nearby taxi services at the lowest prices in Bangalore. Get My Taxi provides the most Reliable , Affordable and Cheapest fare Taxi services in and around Bangalore, We also provide airport pickup, Drop, round trip and outstation cabs services in Bang', 'Cabs in Bangalore, Taxi near me cheap, Bangalore Cabs, Taxi services nearby, Taxi services at lowest price, Taxi Bangalore, taxi service in Bangalore, Cab services in Bangalore, Taxi near me, cab services Bangalore.', '2020-11-28 23:41:43'),
(3, 'Out Station Taxi', 'Outstation', 'Book Outstation Cabs at Best Fares | Hire Outstation Cabs Online | Outstation Cabs Service at Getmytaxi.in', 'Choose from a range of Outstation AC cabs, fares starting at Rs. 9/km. Affordable one way and round trip packages. Now book online at www.getmytaxi.in! Outstation cabs, Cheapest outstation cabs in Bangalore, Outstation cabs Bangalore, Online Taxi Booking,', 'Outstation cabs, Cheapest outstation cabs in Bangalore, Outstation cabs Bangalore, Online Taxi Booking, Best taxi services in Bangalore.', '2020-11-28 23:43:14'),
(4, 'About Get My Taxi', 'About-GetMyTaxi', 'Airport Taxi,Outstation Cab,Local Taxi,Rental Cab,Best Taxi service in Bangalore.', 'Get My Taxi is a Bangalore based company working towards excellence in the Transportation, and believe in pursuing business through innovation and technology. Outstation cab booking,outstation taxi booking,outstation cab service,outstation cab,outstation.', ' Outstation cab booking,outstation taxi booking,outstation cab service,outstation cab,outstation taxi.Best Taxi service in Bangalore.', '2020-11-28 23:54:52'),
(5, 'Contact Us', 'Contact-Us', 'Taxi services in Bangalore,Airport Taxi service in Bangalore,Bangalore Airport Taxi.', 'Get My Taxi Makes Your Ride Hassle Free!!!We Keep in pace with changing needs of customers to make customer Safety and Comfort in Travel. Taxi services in Bangalore,Airport Taxi service in Bangalore,Car rental services in Bangalore,Outstation cabs in Bang', 'Taxi services in Bangalore,Airport Taxi service in Bangalore,Car rental services in Bangalore,Outstation cabs in Bangalore,Bangalore Airport Taxi.', '2020-11-29 04:46:57'),
(6, 'Privacy And Policy', 'Privacy-And-Policy', 'Cheapest outstation cabs in Bangalore,Online Booking, Airport Taxi Booking.', 'Get My Taxi Makes Your Ride Hassle Free!!!We Keep in pace with changing needs of customers to make customer Safety and Comfort in Travel. Online Booking, Airport Taxi Booking, Car rentals, Outstation cabs, Cheapest outstation cabs in Bangalore, Outstation', 'Online Booking, Airport Taxi Booking, Car rentals, Outstation cabs, Cheapest outstation cabs in Bangalore, Outstation cabs Bangalore, Get My Taxi.', '2020-11-29 04:51:33'),
(7, 'Terms And Conditions', 'Terms-And-Conditions', 'Book Cabs Nearby at Best Price | Hire Taxi Nearby Online at Getmytaxi.in', 'Car rentals India, book cabs online, book taxi online, airport taxi India, cabs in India, taxi in India, cabs services in India, Airport taxi services in India, car rental services in India, car rentals, taxi, cabs, Airport, rent.', 'Get My Taxi offers to book cabs nearby your location for best fares. For best taxi service at lowest fares, Book Get My Taxi!', '2020-11-29 04:50:11');

-- --------------------------------------------------------

--
-- Table structure for table `local_taxi`
--

CREATE TABLE `local_taxi` (
  `id` int(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `capacity` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `tdh` varchar(255) NOT NULL,
  `tdk` varchar(255) NOT NULL,
  `ekp` varchar(255) NOT NULL,
  `ehp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `local_taxi`
--

INSERT INTO `local_taxi` (`id`, `img`, `name`, `capacity`, `price`, `tdh`, `tdk`, `ekp`, `ehp`) VALUES
(1, 'gallery/Etios.jpg', 'SEDAN(DZIRE ETIOS)', '4+1', '1000', '4', '40', '13', '150'),
(2, 'gallery/Etios.jpg', 'SEDAN(DZIRE , ETIOS)', '4+1', '1800', '8', '80', '13', '150'),
(3, 'gallery/Ertiga.jpg', 'SUV MINI (ERITIGA)', '6+1', '2500', '8', '80', '14', '170'),
(4, 'gallery/innova.jpg', 'SUV (INNOVA)', '7+1', '2800', '8', '80', '15', '200'),
(5, 'gallery/crysta.jpg', 'SUV (INNOVA CRYSTA)', '7+1', '3000', '8', '80', '15', '200');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_06_05_054431_create_admins_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `outstation_taxi`
--

CREATE TABLE `outstation_taxi` (
  `id` int(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `capacity` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `outstation_taxi`
--

INSERT INTO `outstation_taxi` (`id`, `img`, `name`, `capacity`, `price`) VALUES
(1, 'Etios.jpg', 'SEDAN (DZIRE, ETIOS)', '4+1', '10'),
(2, 'Ertiga.jpg', 'SUV MINI (ERITIGA)', '6+1', '13'),
(3, 'innova.jpg', 'SUV (INNOVA)', '7+1', '15'),
(4, 'crysta.jpg', 'SUV (INNOVA CRYSTA)', '7+1', '16');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `number`, `username`, `password`, `date`, `status`) VALUES
(2, 'Vikash Yadav', '8319278218', 'vikashyadav8412@gmail.com', 'Vikash841236@', '31-Oct-2020', 'Active'),
(3, 'Ananth', '9611673611', 'Ananth', 'Ananth@123', '13-Nov-2020', 'Active'),
(4, 'Aishwwarya M', '7406182704', 'aishu', 'aishu@123', '14-Nov-2020', 'Active'),
(5, 'Manjunath B S', '7406001992', 'manjunath', 'manjunath7406', '14-Nov-2020', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airport_taxi`
--
ALTER TABLE `airport_taxi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dr`
--
ALTER TABLE `dr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `flasher`
--
ALTER TABLE `flasher`
  ADD PRIMARY KEY (`sl.no`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `local_taxi`
--
ALTER TABLE `local_taxi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outstation_taxi`
--
ALTER TABLE `outstation_taxi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `airport_taxi`
--
ALTER TABLE `airport_taxi`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `dr`
--
ALTER TABLE `dr`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flasher`
--
ALTER TABLE `flasher`
  MODIFY `sl.no` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `local_taxi`
--
ALTER TABLE `local_taxi`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `outstation_taxi`
--
ALTER TABLE `outstation_taxi`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
