<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{AirportTaxiController, BookingController , LocalTaxiController,OutStationController,ContactController};
use App\Http\Controllers\Admin\{Homecontroller , Logincontroller , AirportAdminController , LocalAdminController};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/' , [AirportTaxiController::class , 'index'])->name('airport');
Route::get('/Airport' , [AirportTaxiController::class , 'index'])->name('airports');
Route::post('/Airport-Taxi' , [AirportTaxiController::class , 'airportTaxi'])->name('airporttaxi');
Route::post('/Confirm-Booking' , [BookingController::class , 'confirmBooking'])->name('confirmbooking');
Route::get('/Local' , [LocalTaxiController::class , 'index'])->name('local');
Route::post('/Local-Taxi' , [LocalTaxiController::class , 'localTaxi'])->name('localtaxi');
Route::get('/Out-Station' , [OutStationController::class , 'index'])->name('outstation');
Route::post('/Out-Station-Taxi' , [OutStationController::class , 'outStationTaxi'])->name('outstationtaxi');
Route::get('/About-GetMyTaxi' , function(){
	return view('About-GetMyTaxi');
})->name('about');
Route::get('/Contact' , [ContactController::class , 'index'])->name('contact');
Route::post('/Add-Enquiry' , [ContactController::class , 'addEnquiry'])->name('addenquiry');
Route::get('/Privacy-And-Policy' , function(){
	return view('Privacy-And-Policy');
})->name('pap');
Route::get('/Terms-And-Conditions' , function(){
	return view('Terms-And-Conditions');
})->name('tac');

//Admin
Route::group(['prefix' => 'admin'], function() {
	Route::get('/' , [Logincontroller::class , 'loginView'])->name('login');
	Route::post('/login' , [Logincontroller::class , 'postLogin']);
	Route::get('/logout' , [Logincontroller::class , 'logout'])->name('logout');
	Route::middleware(['auth:admin'])->group(function () {
		Route::get('/index' , [Homecontroller::class , 'index'])->name('index'); 
		Route::get('/airport-taxi' , [AirportAdminController::class , 'index'])->name('airportadmin'); 
		Route::get('/add-airport-taxi' , [AirportAdminController::class , 'addAirportTaxi'])->name('addairporttaxi');
		Route::post('/add-airport-taxi' , [AirportAdminController::class , 'storeAirportTaxi'])->name('storeairporttaxi'); 
		Route::get('/edit-airport-taxi/{id}' , [AirportAdminController::class , 'editAirportTaxi'])->name('editairporttaxi'); 
		Route::post('/update-airport-taxi' , [AirportAdminController::class , 'updateAirportTaxi'])->name('updateairporttaxi'); 
		Route::get('/delete-airport-taxi/{id}' , [AirportAdminController::class , 'deleteAirportTaxi'])->name('deleteairporttaxi'); 
		Route::get('/local-taxi' , [LocalAdminController::class , 'index'])->name('localadmin');
		Route::get('/add-local-taxi' , [LocalAdminController::class , 'addLocalTaxi'])->name('addlocaltaxi');
		Route::post('/add-local-taxi' , [LocalAdminController::class , 'storeLocalTaxi'])->name('storelocaltaxi');
		Route::get('/edit-local-taxi/{id}' , [LocalAdminController::class , 'editLocalTaxi'])->name('editlocaltaxi');
		Route::get('/delete-local-taxi/{id}' , [LocalAdminController::class , 'deleteLocalTaxi'])->name('deletelocaltaxi');
	});
});