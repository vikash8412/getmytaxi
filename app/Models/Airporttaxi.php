<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airporttaxi extends Model
{
    use HasFactory;
    protected $table = "airport_taxi";
    protected $fillable = ['img' , 'name' , 'capacity' , 'price_upto_300' , 'price_after_300'];
    public $timestamps = false;
}
