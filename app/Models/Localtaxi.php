<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Localtaxi extends Model
{
    use HasFactory;
    protected $table = 'local_taxi';
    public $timestamps = false;
    protected $fillable = ['img' , 'name' , 'capacity' , 'price' , 'tdh' , 'tdk' , 'ehp' , 'ekp'];
}
