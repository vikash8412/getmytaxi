<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $table = 'booking';
    protected $fillable = ['bid' , 'name' , 'email' ,'number', 'anumber' , 'pickup' , 'drop' ,'date' ,  'time' , 'package' , 'price' , 'tc' ,'bdt','status', 'edate' , 'btype' , 'book_by'];
    public $timestamps = false;
}
