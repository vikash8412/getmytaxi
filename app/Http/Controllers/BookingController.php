<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;
class BookingController extends Controller
{
    function confirmBooking(Request $request){
    	$bid = Booking::get()->count();
    	$booking = Booking::create([
    		"bid"=>$bid,
    		"name"=>$request->name,
    		"email"=>$request->email,
    		"number"=>$request->number,
    		"anumber"=>$request->anumber,
    		"pickup"=>$request->pickup,
    		"drop"=>$request->drop,
    		"date"=>$request->date,
    		"time"=>$request->time,
    		"package"=>$request->package,
    		"price"=>$request->price,
    		"tc"=>$request->tc,
    		"bdt"=>now(),
    		"status"=>'Pending',
    		"edate"=>$request->edate ?? '',
    		"btype"=>'Online',
    		"book_by"=>'Online',
    	]);
    	return redirect()->route('airport')->with(['message' => 'Booking has been done successfully.']);
    }
}
