<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Airporttaxi;
class AirportAdminController extends Controller
{
    function index(){
    	$taxis = Airporttaxi::orderBy('id' , 'DESC')->get();
    	return view('/admin/airport-taxi' , ['taxis'=>$taxis]);
    }
    function addAirportTaxi(){
    	return view('/admin/add-airport-taxi');
    }
    function storeAirportTaxi(Request $request){
    	$request->validate([
    		"img"=>"required|mimes:jpg,jpeg,png|max:2048",
    		"name"=>"required",
    		"capacity"=>"required",
    		"price_upto_45"=>"required",
    		"price_after_45"=>"required",
    	]);
    	$name = $request->file('img')->getClientOriginalName();
        $path = $request->file('img')->store('gallery');
        Airporttaxi::create([
        	"img"=>$path,
        	"name"=>$request->name,
        	"capacity"=>$request->capacity,
        	"price_upto_300"=>$request->price_upto_45,
        	"price_after_300"=>$request->price_after_45,
        ]);

        return redirect()->route('airportadmin')->with(['message'=>'Added successfully']);
    }

    function editAirportTaxi($id){
    	$taxi = Airporttaxi::find($id);
    	return view('/admin/edit-airport-taxi' , ['taxi'=>$taxi]);
    }

    function updateairporttaxi(Request $request){
    	$request->validate([
    		
    		"name"=>"required",
    		"capacity"=>"required",
    		"price_upto_45"=>"required",
    		"price_after_45"=>"required",
    	]);	
    	if(!empty($request->img)){
    		$request->validate([
				"img"=>"required|mimes:jpg,jpeg,png|max:2048",
    		]);
        	$name = $request->file('img')->getClientOriginalName();
        	$path = $request->file('img')->store('gallery');
        }else{
        	$path = $request->old_img;
        }
        Airporttaxi::where('id' , $request->id)->update([
        	"img"=>$path,
        	"name"=>$request->name,
        	"capacity"=>$request->capacity,
        	"price_upto_300"=>$request->price_upto_45,
        	"price_after_300"=>$request->price_after_45,
        ]);
		return redirect()->route('airportadmin')->with(['message'=>'Updated successfully']);
    }

    function deleteAirportTaxi($id){
    	Airporttaxi::find($id)->delete();
    	return redirect()->route('airportadmin')->with(['danger'=>'Deleted successfully']);
    }
}
