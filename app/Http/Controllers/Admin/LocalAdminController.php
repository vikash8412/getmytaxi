<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Localtaxi;
class LocalAdminController extends Controller
{
    function index(){
    	$taxis = Localtaxi::orderBy('id' , 'DESC')->get();
    	return view('/admin/local-taxi' , ['taxis'=>$taxis]);
    }

    function addLocalTaxi(){
    	return view('/admin/add-local-taxi');
    }

    function storeLocalTaxi(Request $request){
    	$request->validate([
    		"img"=>"required|mimes:jpg,jpeg,png|max:2048",
    		"name"=>"required",
    		"capacity"=>"required",
    		"price"=>"required",
    		"tdh"=>"required",
    		"tdk"=>"required",
    		"ehp"=>"required",
    		"ekp"=>"required",
    	]);
    	$name = $request->file('img')->getClientOriginalName();
        $path = $request->file('img')->store('gallery');
    	Localtaxi::create([
    		"img"=>$path,
    		"name"=>$request->name,
    		"capacity"=>$request->capacity,
    		"price"=>$request->price,
    		"tdh"=>$request->tdh,
    		"tdk"=>$request->tdk,
    		"ehp"=>$request->ehp,
    		"ekp"=>$request->ekp,
    	]);

    	return redirect()->route('localadmin')->with(['message'=>'Added successfully']);
    }

    function deleteLocalTaxi($id){
    	Localtaxi::find($id)->delete();
    	return redirect()->route('localadmin')->with(['danger'=>'Deleted successfully']);
    }
}
