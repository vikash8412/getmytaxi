<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class Logincontroller extends Controller
{
    function loginView(){
    	return view('/Admin/login');
    }
    function postLogin(Request $request){
       $request->validate([
           'username'    => 'required',
           'password' => 'required',
       ]);
       if (Auth::guard('admin')->attempt(['username' => $request->username,'password' => $request->password])) {
           return redirect()->route('index');
       }
       return redirect()->back()->with('message','Invalid Credential!');
    }
    public function logout()
	   {
	       Auth::guard('admin')->logout();
	       return redirect()->route('login');
	   }
}
