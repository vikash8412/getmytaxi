<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
class ContactController extends Controller
{
    function index(){
    	return view('Contact');
    }
    function addEnquiry(Request $request){
    	Contact::create([
    		"name"=>$request->name,
    		"email"=>$request->email,
    		"number"=>$request->number,
    		"message"=>$request->message,
    		"date"=>date('d-M-Y'),
    	]);
    	return redirect()->route('airport')->with(['message'=>'Enquiry sent successfully']);
    }
}
