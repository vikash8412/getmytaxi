@extends('master')
@section('content')
        <!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('images/background/3.jpg')}});">
        <div class="auto-container">
            <h1>Hurry Book Now! <br><a href="tel:+918310844790 ">Call <i class="fa fa-phone"></i> +91 83108 44790 </a></h1>
            <div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{route('airport')}}">Home</a></li>
                    <li class="active">Airport Taxi</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    <!--Main Banner-->
	<section style="background:url({{asset('images/gmto.jpg')}});padding: 50px 0 ;background-size: cover;background-attachment: fixed;">
		<div class="auto-container">
			<div class="row">
				<div class="col-md-7">
					@foreach($taxis as $taxi)
					<div class="taxi">
						<div class="row">
							<div class="col-md-3">
								<img src="{{asset('storage/gallery/'.$taxi->img)}}" width="100%">
							</div>
							<div class="col-md-3"><p> {{$taxi->name}} ({{$taxi->capacity}})</p></div>
							<div class="col-md-6"> 
								<center><button class="btn btn-info"> <i class="fa fa-rupee"></i> 
									
									@if($total_distance<45)
									<?php $tp = $taxi->price_upto_300; ?>
									{{$tp}}
									@else
									<?php 
									$td = $total_distance - 45;
									$ed = ceil($td/5);
							   		$ep = $ed*$taxi->price_after_300;
							   		$tp = $taxi->price_upto_300+$ep;
									?>
									{{number_format($tp,2)}}
									@endif
							 </button>
							<button class="btn btn-success" data-toggle="modal" data-target="#myModal{{$taxi->id}}"> Book Now </button></center>
							</div>
						</div>
					</div>
<!-- Modal -->
<div id="myModal{{$taxi->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
		<section class="billing-section" style="padding:0;">
    	<div class="auto-container">
        	<center><img src="{{asset('storage/images/logo.jpeg')}}" width="100%"></center>
        	<!--Billing Form-->
            <div class="billing-form" style="margin-bottom:20px;">
            	<form method="post" action="{{route('confirmbooking')}}">
            		@csrf
                	<!--Column-->
                    <div class="row clearfix">
                    	<input type="hidden" name="package" value="Airport">
                    	<input type="hidden" name="pickup" value="{{$request->pickup}}">
                    	<input type="hidden" name="drop" value="{{$request->drop}}">
                    	<input type="hidden" name="date" value="{{$request->date}}">
                    	<input type="hidden" name="time" value="{{$request->time}}">
                    	<input type="hidden" name="price" value="{{$tp}}">
                    	<input type="hidden" name="tc" value="{{$taxi->name}} - {{$taxi->capacity}}">
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Your Name<span class="req">*</span></div>
                            <div class="field-inner"><input type="text" name="name" placeholder="Your Name" required ></div>
                        </div>                        
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Email Address <span class="req">*</span></div>
                            <div class="field-inner"><input type="email" name="email" value="" placeholder="Your Email Address" required ></div>
                        </div>
                        
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Phone No. <span class="req">*</span></div>
                            <div class="field-inner"><input type="number" name="number" placeholder="Phone No." required></div>
                        </div> 

						<div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Alternate Phone No.</div>
                            <div class="field-inner"><input type="number" name="anumber" placeholder="Alternate Phone No."></div>
                        </div> 	

						<center><div class="btn-outer"><button type="submit" class="theme-btn btn-style-three">Confirm Booking</button></div></center>						
                    </div>
                </form>
            </div>            
        </div>
    </section>
      </div>
    </div>
  </div>
</div>					

					@endforeach
				</div>
				<div class="col-md-5">
					<div class="content">
						<h1>Airport Taxi at your service  Starts @ 799/-  Within 45 kms of City.</h1>
						<ul>
							<li>Book Airport Round Ride (To and Fro) Starts @ 1399/- Within 45 kms of City.</li>
							<li>Timely pickup and drop . </li>

<li>No Hidden Charges – Only Fixed Charges . </li>

<li>No Cancellation Charges  - No Waiting Charges</li>

<li>GPS Enabled Vehicles . </li>

<li>Pre-Checked and Verified Drivers . </li>

<li>Cabs are Well Sanitized thoroughly before and after every trip . </li>

<li>Good Maintained & Cleaned Cabs . </li>

<li>24x7 customer support</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!--End Main Banner-->	
@endsection