
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title> GetMyTaxi</title>
<meta name="keywords" content="GetMyTaxi">
<meta name="description" content="GetMyTaxi">
<!-- Stylesheets -->
<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('css/revolution-slider.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<link href="{{asset('css/style.css')}}" rel="stylesheet">
<link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
<link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link href="{{asset('css/responsive.css')}}" rel="stylesheet">
</head>
<body>
<div class="page-wrapper">  
    <!-- Main Header-->
	<header class="main-header header-style-two">
    	<!-- Header Top -->
    	<div class="header-top">
        	<div class="auto-container">
            	<div class="clearfix">
                    
                    <!--Top Right-->
                    <div class="top-left">
                    	<!--social-icon-->
                        <div class="social-icon">
                        	<a href="https://www.facebook.com/Get-My-Taxi-100649348516659" target="blank"><span class="fa fa-facebook"></span></a>
                            <a href="https://twitter.com/GetMyTaxi1" target="blank"><span class="fa fa-twitter" ></span></a>
                            <a href="https://www.instagram.com/info.getmytaxi/?fbclid=IwAR2zM80_pwvX6kmvPYI_iQG0xC7Eycod51fGJLzZIH1PR1ZybJToFfHSChw" target="blank"><span class="fa fa-instagram"></span></a>
                            <a href="https://www.linkedin.com/company/69648828/admin/" target="blank"><span class="fa fa-linkedin"></span></a>
                            
                        </div>
                    </div>
                    
                    <!--Top Left-->
                    <div class="top-right">
                    	<ul class="clearfix">
                        	<li><span class="icon flaticon-technology-1"></span>+918884488255 / +918884488355</li>
                            <li><span class="icon flaticon-envelope-3"></span>info@getmytaxi.in</li>
                        </ul>
                    </div>
                    
                </div>
                
            </div>
        </div>
        <!-- Header Top End -->
        
        <!-- Main Box -->
    	<div class="main-box">
        	<div class="auto-container">
            	<div class="outer-container clearfix">
                    <!--Logo Box-->
                    <div class="logo-box">
                        <div class="logo"><a href="{{route('airport')}}"><img src="images/logo.jpeg" alt=""></a></div>
                    </div>
                    
                    <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->    	
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="{{route('airports')}}">Airport</a></li>
                                    <li><a href="{{route('local')}}">Local</a></li>
                                    <li><a href="{{route('outstation')}}">Out Station</a></li>
									<li><a href="{{route('about')}}">About Get My Taxi</a></li>
                                    <li><a href="{{route('contact')}}">Contact Us</a></li>                                   
                                 </ul>
                            </div>
                        </nav><!-- Main Menu End-->
                        
                    </div>
                    <!--Nav Outer End-->
                    
                    <!-- Hidden Nav Toggler -->
                    <div class="nav-toggler">
                    <button class="hidden-bar-opener"><span class="icon fa fa-bars"></span></button>
                    </div><!-- / Hidden Nav Toggler -->
                    
            	</div>    
            </div>
        </div>
    
    </header>
    <!--End Main Header -->
    
    <!-- Hidden Navigation Bar -->
    <section class="hidden-bar right-align">
        
        <div class="hidden-bar-closer">
            <button class="btn"><i class="fa fa-close"></i></button>
        </div>
        
        <!-- Hidden Bar Wrapper -->
        <div class="hidden-bar-wrapper">
        
            <!-- .logo -->
            <div class="logo text-center">
                <a href="index"><img src="images/logo.jpeg" alt="Get My Taxi"></a>			
            </div><!-- /.logo -->
            
            <!-- .Side-menu -->
            <div class="side-menu">
            <!-- .navigation -->
            <ul class="navigation clearfix">
                <li><a href="index">Airport</a></li>
                <li><a href="Local">Local</a></li>
                <li><a href="Out-Station">Out Station</a></li>
				<li><a href="About-GetMyTaxi">About Get My Taxi</a></li>
                <li><a href="Contact-Us">Contact Us</a></li>
             </ul>
            </div><!-- /.Side-menu -->
        
            <div class="social-icons">
                <ul>
                    <li><a href="https://www.facebook.com/Get-My-Taxi-100649348516659"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/GetMyTaxi1"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.instagram.com/info.getmytaxi/?fbclid=IwAR2zM80_pwvX6kmvPYI_iQG0xC7Eycod51fGJLzZIH1PR1ZybJToFfHSChw"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.linkedin.com/company/69648828/admin/"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        
        </div><!-- / Hidden Bar Wrapper -->
    </section>
    <!-- / Hidden Bar -->