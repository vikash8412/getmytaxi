@extends('master')
@section('content')
    <section class="page-title" style="background-image:url(images/background/3.jpg);">
        <div class="auto-container">
            <h1>Hurry Book Now! <br><a href="tel:+918310844790 ">Call <i class="fa fa-phone"></i> +91 83108 44790 </a></h1>
            <div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{route('airport')}}">Home</a></li>
                    <li class="active">Out Station Taxi</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    <!--Main Banner-->
	<section style="background:url('images/gmto.jpg');padding: 50px 0 ;background-size: cover;background-attachment: fixed;">
		<div class="auto-container">
			<div class="row">
				<div class="col-md-7">
				<div style="background: #f2b341;
    color: white;
    text-align: center;
    padding: 10px;">
					<span style="font-size:18px;"> <b>Out Station Car Hire : </b> 
				{{$request->pickup}} <i class="fa fa-arrow-right"></i> {{$request->drop}}</span>
				<br>
				<i class="fa fa-calendar"></i> {{$request->sdate}} <i class="fa fa-arrow-right"></i> {{$request->edate}} | Approximate Travel Distance <span style="color:black;">{{$total_distance}} </span> KM  <?php  
$start_date = strtotime($request->sdate);
$end_date = strtotime($request->edate);
$datediff =  $end_date - $start_date;
$newdate =  round($datediff / (60 * 60 * 24));
$total_days = $newdate+1;
 ?>
				<br>
				<hr>
				</div>	
				@foreach($taxis as $taxi)
					<div class="taxi" style="padding: 20px 20px 10px 20px;">

						<div class="row">
							<div class="col-md-3">
								
								<img src="{{asset('storage/gallery/'.$taxi->img)}}" width="100%">
							</div>
							<div class="col-md-6">
							<h4 style="font-weight: bold;font-size: 14px;margin-bottom: 6px;">{{$taxi->name}} ({{$taxi->capacity}})</h4>
							<span class="small">₹{{number_format($taxi->price , 2)}}/KM</span>
						   <h2 style="font-size: 20px;">Est Fare  ₹
						   <?php if($total_distance<300){
							   echo $price = $taxi->price * $total_distance + 300 * $total_days;
						   }else{
							   echo $price = $total_distance*$taxi->price+300*$total_days;
						   } ?></h2>
                            
						   <span class="small"><a data-toggle="modal" data-target="#fb{{$taxi->id}}">Fare Breakup</a></span></div>
							<div class="col-md-3"> 
							<button class="btn btn-success" data-toggle="modal" data-target="#myModal{{$taxi->id}}"> Book Now </button></center>
							</div>
						</div>
						
	<!-- Modal -->
<div id="fb{{$taxi->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Fare Breakup</h4>
      </div>
      <div class="modal-body">
		<table width="100%" cellpadding="3" cellspacing="1" class="text_11b">
        
        
        <tbody><tr>
          <td width="52%"> Rate per KM <i class="fa fa-rupee"></i> {{$taxi->price}} </td>
        </tr>
        <tr>
          <td> Driver Bata 300</td>
        </tr>
        <tr>
          <td> Journey Days : {{$total_days}} days </td>
        </tr>
        <tr>
          <td class="border_bottom2"> Estimated Distance : {{$total_distance}} KM</td>
        </tr>
        
        <tr>
          <td><span class="text_h_up_vehicle style6">Fare Calculation </span></td>
        </tr>
        <tr>
          <td>({{$taxi->price}} X {{$total_distance}}  ) + (300 X {{$total_days}} )							  
							  &nbsp;&nbsp;</td>
        </tr>
        <tr>
          <td><strong>Total Estimated Fare: ₹
          <?php if($total_distance<300){
							   echo $price = $taxi->price*$total_distance + 300 * $total_days;
						   }else{
							   echo $price = $total_distance*$taxi->price+300*$total_days;
						   } ?>          &nbsp;</strong></td>
        </tr>
        <tr>
          <td style="font-size:12px;"><strong>
          Notes</strong> 
            <ul style="font-size:13px;">
              <li>Inter-state tax, Toll Charge and Parking as applicable</li>
              <li>Driver Night charges ( Rs. 300 per night) if driver on duty between 10PM to 6 AM</li>
              <li>Minimum km chargeable per day is 300 kms</li>
          </ul>            </td></tr>
        </tbody></table>
      </div>
    </div>

  </div>
</div>
<div id="myModal{{$taxi->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
		<section class="billing-section" style="padding:0;">
    	<div class="auto-container">
        	<center><img src="images/logo.jpeg" width="100%"></center>
        	<!--Billing Form-->
            <div class="billing-form" style="margin-bottom:20px;">
            	<form method="post" action="{{route('confirmbooking')}}">
                @csrf
						<input type="hidden" name="package" value="Outstation">
                      <input type="hidden" name="package" value="Airport">
                      <input type="hidden" name="pickup" value="{{$request->pickup}}">
                      <input type="hidden" name="drop" value="{{$request->drop}}">
                      <input type="hidden" name="date" value="{{$request->sdate}}">
                      <input type="hidden" name="edate" value="{{$request->edate}}">
                      <input type="hidden" name="time" value="{{$request->time}}">
                      <input type="hidden" name="price" value="{{$price}}">
                      <input type="hidden" name="tc" value="{{$taxi->name}} - {{$taxi->capacity}}">
                	<!--Column-->
                    <div class="row clearfix">
                    	
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Your Name<span class="req">*</span></div>
                            <div class="field-inner"><input type="text" name="name" placeholder="Your Name" required ></div>
                        </div>                        
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Email Address <span class="req">*</span></div>
                            <div class="field-inner"><input type="email" name="email"  placeholder="Your Email Address" required ></div>
                        </div>
                        
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Phone No. <span class="req">*</span></div>
                            <div class="field-inner"><input type="number" name="number" placeholder="Phone No." required ></div>
                        </div> 

						<div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Alternate Phone No.</div>
                            <div class="field-inner"><input type="number" name="anumber" placeholder="Alternate Phone No."></div>
                        </div> 	

						<center><div class="btn-outer"><button type="submit" class="theme-btn btn-style-three">Confirm Booking</button></div></center>						
                    </div>
                </form>
            </div>            
        </div>
    </section>
      </div>
    </div>

  </div>
</div>
					</div>
					
				@endforeach
				</div>
				
				<div class="col-md-5">
					<div class="content">
						<h1>Get My Taxi Outstation Starts @ 9/- Per Km.</h1>
						<ul>
							<li>Get My Taxi Offers Outstation affordable prices</li>
							<li>Interstate Charges applicable as per state.</li>

<li>GPS Enabled Vehicles . </li>

<li>Pre-Checked and Verified Drivers . </li>

<li>Cabs are Well Sanitized thoroughly before and after every trip . </li>

<li>Good Maintained & Cleaned Cabs . </li>

<li>24x7 customer support</li>

<li>Timely pickup and drop</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
  @endsection