@extends('master')
@section('content')
    <section class="page-title" style="background-image:url(images/background/3.jpg);">
        <div class="auto-container">
            <h1>CONTACT US</h1>
            <div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{route('airport')}}">Home</a></li>
                    <li class="active">CONTACT US</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->    
    <!--Contact Section -->
    <div class="contact-section" style="    padding: 50px 0px 0px;">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Map Side--> 
                <div class="map-column column col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    
                    <!--Map Section-->
                    <section class="map-section">
                        <div class="map-outer">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3889.0315282792703!2d77.5940547141346!3d12.905694219814503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1510a9cf9a6b%3A0xca1c500378b13d4b!2s7th%20B%20Main%20Rd%20%26%2016th%20Cross%20Rd%2C%20JP%20Nagar%204th%20Phase%2C%20Dollar%20Layout%2C%20Phase%204%2C%20J.%20P.%20Nagar%2C%20Bengaluru%2C%20Karnataka%20560078!5e0!3m2!1sen!2sin!4v1603588531966!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </section>
                </div>
                
                <!--Form Column-->  
                <div class="form-column column col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    
                    <!-- Contact Form -->
                    <div class="contact-form">
                        <div class="group-title">
                            <h3>DROP US A LINE</h3>
                            <div class="text"></div>
                        </div>
                        <!--Contact Form-->
                        <form method="post" action="{{route('addenquiry')}}" id="contact-form">
                            @csrf
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="name" placeholder="Name *">
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="email" name="email" placeholder="Email *">
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="number" name="number" placeholder="Phone No. *">
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <textarea name="message" placeholder="Message"></textarea>
                                </div>
                                
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <button class="theme-btn btn-style-one" type="submit" >SUBMIT</button>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                    <!--End Contact Form -->
                    
                </div>
                
            </div>
        </div>
    </div>
    @endsection