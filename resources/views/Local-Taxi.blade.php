@extends('master')
@section('content')
<!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/3.jpg);">
        <div class="auto-container">
            <h1>Hurry Book Now! <br><a href="tel:+918310844790 ">Call <i class="fa fa-phone"></i> +91 83108 44790 </a></h1>
            <div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="{{route('airport')}}">Home</a></li>
                    <li class="active">Local Taxi</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
<div class="sidebar-page-container" style="background:url('images/gmto.jpg');padding: 50px 0 ;background-size: cover;background-attachment: fixed;">
    	<!--Tabs Box-->
        <div class="container">
            <div class="row">
                
                <!--Content Side-->	
                <div class="col-md-7">
                    <!--Cars Info Section-->
                    <section class="cars-info-section grid-view">                       
                        <div class="row clearfix">                       
                        	<!--car info block-->
                            @foreach($taxis as $taxi)
                        	<div class="car-info-block col-md-12 col-sm-12 col-xs-12">
                            	<div class="inner-box">
									<div class="row">
										<div class="col-md-3">
											<div class="image" style="margin-top:65px;">
												<a><img src="{{asset('storage/gallery/'.$taxi->img)}}" alt=""></a>
											</div>
										</div>
										<div class="col-md-9">
											<div class="lower-box">
												<h3><a>{{$taxi->name}}({{$taxi->capacity}})</a></h3>
												<div class="more-info">
													<ul>
														<li><i class="fa fa-user"></i> <span> {{$taxi->capacity}} Seating </span></li>
														<li><i class="fa fa-gift"></i> <span> {{$taxi->tdh}} Hrs {{$taxi->tdk}} Kms</span></li>
														<li><i class="fa fa-bell"></i> <span>Extra KM <i class="fa fa-rupee"></i>{{$taxi->ekp}} Extra HR <i class="fa fa-rupee"></i>{{$taxi->ehp}}</span></li>
													</ul>
													<div class="price-day" style="text-align: center;"><i class="fa fa-rupee"></i>{{$taxi->price}}<span></span><br>
													<button type="submit" class="theme-btn btn-style-one" style="padding: 5px 30px;margin-top: 10px;border-radius: 5px;" data-toggle="modal" data-target="#myModal{{$taxi->id}}">BOOK NOW</button>
													</div>
												</div>
											</div>
										</div>
									</div>
                                	
	<!-- Modal -->
<div id="myModal{{$taxi->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
		<section class="billing-section" style="padding:0;">
    	<div class="auto-container">
        	<center><img src="images/logo.jpeg" width="100%"></center>
        	<!--Billing Form-->
            <div class="billing-form" style="margin-bottom:20px;">
            	<form method="POST" action="{{route('confirmbooking')}}">
                    @csrf
						<input type="hidden" name="package" value="Airport">
                        <input type="hidden" name="pickup" value="{{$request->pickup}}">
                        <input type="hidden" name="drop" value="{{$request->drop}}">
                        <input type="hidden" name="date" value="{{$request->date}}">
                        <input type="hidden" name="time" value="{{$request->time}}">
                        <input type="hidden" name="price" value="{{$taxi->price}}">
                        <input type="hidden" name="tc" value="{{$taxi->name}} - {{$taxi->capacity}}">
                	<!--Column-->
                    <div class="row clearfix">
                    	
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Your Name<span class="req">*</span></div>
                            <div class="field-inner"><input type="text" name="name" placeholder="Your Name" required ></div>
                        </div>                        
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Email Address <span class="req">*</span></div>
                            <div class="field-inner"><input type="email" name="email"  placeholder="Your Email Address" required ></div>
                        </div>
                        
                        <div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Phone No. <span class="req">*</span></div>
                            <div class="field-inner"><input type="number" name="number" placeholder="Phone No." required ></div>
                        </div> 

						<div class="form-group col-md-6 col-sm-12 col-xs-12">
                        	<div class="field-label">Alternate Phone No.</div>
                            <div class="field-inner"><input type="number" name="anumber" placeholder="Alternate Phone No."></div>
                        </div> 	

						<center><div class="btn-outer"><button type="submit" class="theme-btn btn-style-three">Confirm Booking</button></div></center>						
                    </div>
                </form>
            </div>            
        </div>
    </section>
      </div>
    </div>

  </div>
</div>
                                </div>
                            </div>
							@endforeach                          
                        </div>
                        
                    </section>
                    
                </div>

                <div class="col-md-5">
					<div class="content">
						<h1>Bangalore Local Drop Taxi Services  - Within Bangalore City</h1>
						<ul>
							<li> Get My Taxi Offers Local Packages affordable prices  </li>

<li> No Hidden Charges – Only Fixed Charges  </li>

<li> No Cancellation Charges  -No Waiting Charges  </li>

<li> GPS Enabled Vehicles  </li>

<li> Pre-Checked and Verified Drivers  </li>

<li> Cabs are Well Sanitized thoroughly before and after every trip  </li>

<li> Good Maintained & Cleaned Cabs  </li>

<li> 24x7 customer support  </li>

<li> Timely pickup and drop </li>
						</ul>
					</div>
				</div>
                
            </div>
        </div>
    </div>
    @endsection